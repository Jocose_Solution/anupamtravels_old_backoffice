﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="false" CodeFile="HotelRefundReport.aspx.vb" Inherits="DetailsPort_Refund_HotelRefundReport" %>

<%@ Register Src="~/UserControl/HotelMenu.ascx" TagPrefix="uc1" TagName="HotelMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%=ResolveUrl("~/Hotel/css/B2Bhotelengine.css") %>" rel="stylesheet" type="text/css" />
      <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <%--<link href="../../CSS/main2.css" rel="stylesheet" type="text/css" />--%>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/gridview-readonly-script.js")%>"></script>
    <script src="<%=ResolveUrl("~/Hotel/JS/HotelRefund.js")%>" type="text/javascript"></script>


<%--    <div class="mtop80"></div>
    <div class="large-12 medium-12 small-12">

        <div class="large-3 medium-3 small-12 columns">

            <uc1:HotelMenu runat="server" ID="Settings"></uc1:HotelMenu>

        </div>

        <div class="large-8 medium-8 small-12 columns end">
            <div class="large-12 medium-12 small-12 heading">
                <div class="large-12 medium-12 small-12 heading1">
                    Search Cancelled hotel
                </div>

                <div class="clear1"></div>
                <div class="large-2 medium-2 small-3 columns">
                    From date
                </div>
                <div class="large-3 medium-3 small-9  columns">
                    <input type="text" name="From" id="From" readonly="readonly" class="txtCalander" />
                </div>
                <div class="large-2 medium-2 small-3 large-push-2 medium-push-2 columns">
                    To date
                </div>
                <div class="large-3 medium-3 small-9 large-push-2 medium-push-2 columns">
                    <input type="text" name="To" id="To" readonly="readonly" class="txtCalander" />
                </div>
                <div class="clear"></div>
                <div class="large-2 medium-2 small-3 columns">
                    Checkin
                </div>
                <div class="large-3 medium-3 small-9  columns">
                    <input type="text" name="Checkin" id="Checkin" class="txtCalander" />
                </div>
                <div class="large-2 medium-2 small-9 large-push-2 medium-push-2 columns">
                    Trip type
                </div>
                <div class="large-3 medium-3 small-9 large-push-2 medium-push-2 columns">
                    <asp:DropDownList ID="Triptype" runat="server">
                        <asp:ListItem Text="Select trip type" Value=""></asp:ListItem>
                        <asp:ListItem Text="Domestic" Value="Domestic"></asp:ListItem>
                        <asp:ListItem Text="International" Value="International"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="clear"></div>

                <div class="large-2 medium-3 small-3 columns">
                    Order ID
                </div>
                <div class="large-3 medium-3 small-9 columns">
                    <asp:TextBox ID="txt_OrderId" runat="server"></asp:TextBox>
                </div>
                <div class="large-2 medium-2 small-9 large-push-2 medium-push-2 columns">
                    Booking ID
                </div>
                <div class="large-3 medium-3 small-9 large-push-2 medium-push-2 columns">
                    <asp:TextBox ID="txt_bookingID" runat="server"></asp:TextBox>
                </div>
                <div class="clear"></div>
                <div class="large-2 medium-2 small-3 columns">
                    Hotel name
                </div>
                <div class="large-3 medium-3 small-9 columns">
                    <asp:TextBox ID="txt_htlcode" runat="server"></asp:TextBox>
                </div>
                <div class="large-2 medium-2 small-3 large-push-2 medium-push-2 columns">
                    Room name
                </div>
                <div class="large-3 medium-3 small-9 large-push-2 medium-push-2 columns">
                    <asp:TextBox ID="txt_roomcode" runat="server"></asp:TextBox>
                </div>
                <div class="clear"></div>
                <div id="TDAgency" runat="server">

                    <div class="large-2 medium-2 small-3 columns">
                        Agency Name
                    </div>
                    <div class="large-3 medium-3 small-9 columns">
                        <input type="text" id="txtAgencyName" name="txtAgencyName" onfocus="focusObj(this);"
                            onblur="blurObj(this);" value="Agency Name or ID" />
                        <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                    </div>
                    <div class="clear"></div>

                    <div id="tr_ExecID" runat="server">

                        <div class="large-2 medium-2 small-3 columns">
                            Exec ID
                        </div>
                        <div class="large-3 medium-3 small-9 columns">
                            <asp:DropDownList ID="ddl_ExecID" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="large-2 medium-2 small-3 large-push-2 medium-push-2 columns">
                            Status
                        </div>
                        <div class="large-3 medium-3 small-9 columns">
                            <asp:DropDownList ID="ddl_Status" runat="server">
                                <asp:ListItem Text="Select Status" Value=""></asp:ListItem>
                                <asp:ListItem Text="Cancelled" Value="Cancelled"></asp:ListItem>
                                <asp:ListItem Text="InProcess" Value="InProcess"></asp:ListItem>
                                <asp:ListItem Text="Pending" Value="Pending"></asp:ListItem>
                            </asp:DropDownList>
                        </div>

                    </div>
                    <div class="clear"></div>

                </div>

                <div class="large-8 medium-8 small-12 columns" style="font-size: 11px; line-height: 20px; text-align: justify; color: Red;">
                    * To get today booking, do not fill any field, only click on Search Result Button.
                </div>
                <div class="large-4 medium-4 small-12 columns">
                    <div class="large-7 medium-7 small-6 columns">
                        <asp:Button ID="btn_result" runat="server" Text="Search Result" CssClass="button" /></div>


                    <div class="large-5 medium-5 small-6 columns">
                        <asp:Button ID="btn_export" runat="server" CssClass="button" Text="Export" /></div>
                </div>
                <div class="clear1"></div>
            </div>
            <div class="clear"></div>
        </div>

        <div class="large-6 medium-6 small-12 large-push-4 medium-push-4">
            <div style="float: left;">
                Total Refund Amount :
                                <asp:Label ID="lbl_Total" runat="server"></asp:Label>
            </div>
            <div style="float: right; margin-left: 40px;">
                Total Hotel Cancelled :
                                <asp:Label ID="lbl_counttkt" runat="server"></asp:Label>
            </div>
        </div>

        <div class="clear1"></div>
        
    </div>
    <div class="large-10 medium-10 small-12 large-push-1 medium-push-1" align="center">
            <asp:GridView ID="grd_RefundReport" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                PageSize="40" CssClass="mGrid" BackColor="White" BorderColor="#3366CC" BorderStyle="None">
                <Columns>
                    <asp:TemplateField HeaderText="Credit Node">
                        <ItemTemplate>
                            <a href='../Accounts/HotelCreditNote.aspx?OrderId=<%#Eval("CancID") %>&amp;AgentID=<%#Eval("BookedBy") %>' rel="lyteframe"
                                rev="width: 920px; height: 407px; overflow:hidden;" target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">Invoice</a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="BookingID" HeaderText="BookingID" ReadOnly="true" />
                    <asp:BoundField DataField="Status" HeaderText="Status" />
                    <asp:BoundField DataField="BookedBy" HeaderText="Agent ID " />
                    <asp:BoundField DataField="AgencyName" HeaderText="Agency Name" />
                    <asp:BoundField DataField="TotalCost" HeaderText="Total Cost" />
                    <asp:BoundField DataField="NetCost" HeaderText="Cost to Agent" />
                    <asp:BoundField DataField="RefundFare" HeaderText="Refund Fare" />
                    <asp:BoundField DataField="CancelCharge" HeaderText="Cancel Charge" />
                    <asp:BoundField DataField="ServiceCharge" HeaderText="Service Charge" />
                    <asp:TemplateField HeaderText="DETAIL">
                        <ItemTemplate>
                            <div class="tag">
                                <a href="#" class="gridViewToolTip">
                                    <img src="<%=ResolveUrl("~/Images/view_icon.gif")%>" border="0" /></a>
                                <div id="tooltip" style="display: none;">
                                    <div style="float: left;">
                                        <table width="100%" cellpadding="11" cellspacing="11" border="0">
                                            <tr>
                                                <td style="width: 110px; font-weight: bold;">Hotel Name:
                                                </td>
                                                <td style="width: 166px;">
                                                    <%#Eval("HotelName")%>  
                                                </td>
                                                <td style="width: 110px; font-weight: bold;">CheckIn Date:
                                                </td>
                                                <td style="width: 166px;">
                                                    <%#Eval("CheckIN")%>  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 110px; font-weight: bold;">Room Name:
                                                </td>
                                                <td style="width: 166px;">
                                                    <%#Eval("RoomName")%>  
                                                </td>

                                                <td style="width: 110px; font-weight: bold;">ChekOut Date:
                                                </td>
                                                <td style="width: 166px;">
                                                    <%#Eval("CheckOut")%>  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 110px; font-weight: bold;">Trip Type:
                                                </td>
                                                <td style="width: 166px;">
                                                    <%#Eval("TripType")%>  
                                                </td>
                                                <td style="width: 110px; font-weight: bold;">Booking Date:
                                                </td>
                                                <td style="width: 166px;">
                                                    <%#Eval("BookingDate")%>  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 110px; font-weight: bold;">Star Rating:
                                                </td>
                                                <td style="width: 166px;">
                                                    <%#Eval("StarRating")%> 
                                                </td>
                                                <td style="width: 110px; font-weight: bold;">Executive ID:
                                                </td>
                                                <td style="width: 166px;">
                                                    <%#Eval("ExecutiveID")%>  
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remark">
                        <ItemTemplate>
                            <div class="tag">
                                <a href="#" class="gridViewToolTip">
                                    <img src="<%=ResolveUrl("~/Images/view_icon.gif")%>" border="0" /></a>
                                <div id="tooltip" style="display: none;">
                                    <div style="float: left;">
                                        <table width="100%" cellpadding="11" cellspacing="11" border="0">
                                            <tr>
                                                <td style="width: 130px; font-weight: bold;">Request Remark:
                                                </td>
                                                <td style="width: 166px;">
                                                    <%#Eval("RequestRemark")%>  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 130px; font-weight: bold;">Request Date:
                                                </td>
                                                <td style="width: 166px;">
                                                    <%#Eval("RequestDate")%>  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 130px; font-weight: bold;">Accept Date:
                                                </td>
                                                <td style="width: 166px;">
                                                    <%#Eval("AcceptDate") %> 
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 130px; font-weight: bold;">Updated Remark:
                                                </td>
                                                <td style="width: 166px;">
                                                    <%#Eval("UpdateRemark")%>  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 130px; font-weight: bold;">Updated Date:
                                                </td>
                                                <td style="width: 166px;">
                                                    <%#Eval("UpdateDate") %>  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 130px; font-weight: bold;">Reject Remark:
                                                </td>
                                                <td style="width: 166px;">
                                                    <%#Eval("RejectRemark")%>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>--%>

  
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>



     <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
    <div class="row">
 <%--    <div class="col-md-11 pull-right" style="padding:30px 30px 30px 100px;">--%>
          <div class="col-md-11 pull-right">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading"  style="background: #fdb714">
                        <h3 class="panel-title"> Hotel > Search Cancelled hotel </h3>
                    </div>
                    <div class="panel-body">


                          <div class="row">
                         <div class="col-md-3">
                            <div class="form-group">
                            <label for="exampleInputPassword1">From date</label>
                          <input type="text" name="From" id="From" readonly="readonly" class="input-text full-width" />
                            </div>
                             </div>
                             
                             <div class="col-md-3">
                                 <div class="form-group">
                            <label for="exampleInputPassword1">To date</label>
                               <input type="text" name="To" id="To" readonly="readonly" class="input-text full-width" />
                            </div>
                             </div>

                         <div class="col-md-3">
                             <div class="form-group">
                            <label for="exampleInputPassword1">Checkin</label>
                            <input type="text" class="input-text full-width" id="Checkin" name="Checkin"  />
                            </div>
                        </div>
                            </div>

                        <div class="row">
                         <div class="col-md-3">
                            <div class="form-group">
                            <label for="exampleInputPassword1">Trip type</label>
                            <asp:DropDownList ID="Triptype" CssClass="input-text full-width" runat="server">
                                            <asp:ListItem Text="Select trip type" Value=""></asp:ListItem>
                                            <asp:ListItem Text="Domestic" Value="Domestic"></asp:ListItem>
                                            <asp:ListItem Text="International" Value="International"></asp:ListItem>
                                        </asp:DropDownList>
                            </div>
                             </div>

                             
                                     <div class="col-md-3">
                             <div class="form-group">
                            <label for="exampleInputPassword1">Order ID</label>
                            <asp:TextBox CssClass="input-text full-width" runat="server" ID="txt_OrderId"  ></asp:TextBox>
                            </div>
                           </div>



                                     <div class="col-md-3">
                             <div class="form-group">
                            <label for="exampleInputPassword1">Booking ID</label>
                            <asp:TextBox CssClass="input-text full-width" runat="server" ID="txt_bookingID"  ></asp:TextBox>
                            </div>
                           </div>

                              </div>

                        <div class="row">
                           <div class="col-md-3">
                             <div class="form-group">
                            <label for="exampleInputPassword1">Hotel name</label>
                            <asp:TextBox CssClass="input-text full-width" runat="server" ID="txt_htlcode"  ></asp:TextBox>
                            </div>
                           </div>
                                     <div class="col-md-3">
                             <div class="form-group">
                            <label for="exampleInputPassword1">Room name</label>
                            <asp:TextBox CssClass="input-text full-width" runat="server" ID="txt_roomcode"  ></asp:TextBox>
                            </div>
                           </div>

                              <div class="col-md-3">
                             <div class="form-group" id="TDAgency" runat="server">
                            <label for="exampleInputPassword1">Agency Name</label>
                            <asp:TextBox CssClass="input-text full-width" id="txtAgencyName" runat="server" name="txtAgencyName"  onfocus="focusObj(this);"
                                            onblur="blurObj(this);" value="Agency Name or ID"  ></asp:TextBox>
                                  <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                            </div>
                           </div>
                           
                            </div>



                         <div class="row">
                              <div id="tr_ExecID" runat="server">
                             <div class="col-md-3">
                          <div class="form-group">
                       
                        <label for="exampleInputPassword1"> Exec ID</label>                     
                            <asp:DropDownList ID="ddl_ExecID" CssClass="input-text full-width" runat="server">
                            </asp:DropDownList>
                        </div>
                              </div>
                             


                             <div class="col-md-3" >
                          <div class="form-group">
                        <label for="exampleInputPassword1">  Status</label>       
                            
                            <asp:DropDownList ID="ddl_Status" CssClass="input-text full-width" runat="server">
                                <asp:ListItem Text="Select Status" Value=""></asp:ListItem>
                                <asp:ListItem Text="Cancelled" Value="Cancelled"></asp:ListItem>
                                <asp:ListItem Text="InProcess" Value="InProcess"></asp:ListItem>
                                <asp:ListItem Text="Pending" Value="Pending"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                           </div>
                                  </div>

                    </div>
                           
                        


                         <div >
                      
                         
                                <asp:Button ID="btn_result" runat="server"  Text="Search Result" CssClass="btn btn-info full-width btn-medium"   />
                            <hr />                   
                                <asp:Button ID="btn_export" runat="server"  Text="Export" CssClass="btn btn-info full-width btn-medium" />
                       
                             </div>

                          <div class="row">
                             <div class="col-md-8">
                           <div class="form-group" style="font-size: 15px; line-height: 20px; text-align: justify; color: Red;">* To get today booking, do not fill any field, only click on Search Result Button.
                                    </div>
                                 </div>
                              </div>
                                

                               <div class="row">
                             <div class="col-md-4">
                           <div class="form-group">

                                <label for="exampleInputPassword1">Total Refund Amount :</label>
                                <asp:Label ID="lbl_Total" runat="server"></asp:Label>
                               </div>
                               </div>
                              </div>

                                   <div class="row">
                             <div class="col-md-4">
                           <div class="form-group">
                                <label for="exampleInputPassword1">Total Hotel Cancelled :</label>                                
                                <asp:Label ID="lbl_counttkt" runat="server"></asp:Label>   
                              
                               </div>
                                 </div>
                               </div>



                                 <div class="col-md-12">
                            <asp:UpdatePanel runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="grd_RefundReport" runat="server" AutoGenerateColumns="False"  PageSize="8"
                                        CssClass="table" GridLines="None" Width="100%">
                                          <Columns>
                    <asp:TemplateField HeaderText="Credit Node">
                        <ItemTemplate>
                            <a href='../Accounts/HotelCreditNote.aspx?OrderId=<%#Eval("CancID") %>&amp;AgentID=<%#Eval("BookedBy") %>' rel="lyteframe"
                                rev="width: 920px; height: 407px; overflow:hidden;" target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">Invoice</a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="BookingID" HeaderText="BookingID" ReadOnly="true" />
                    <asp:BoundField DataField="Status" HeaderText="Status" />
                    <asp:BoundField DataField="BookedBy" HeaderText="Agent ID " />
                    <asp:BoundField DataField="AgencyName" HeaderText="Agency Name" />
                    <asp:BoundField DataField="TotalCost" HeaderText="Total Cost" />
                    <asp:BoundField DataField="NetCost" HeaderText="Cost to Agent" />
                    <asp:BoundField DataField="RefundFare" HeaderText="Refund Fare" />
                    <asp:BoundField DataField="CancelCharge" HeaderText="Cancel Charge" />
                    <asp:BoundField DataField="ServiceCharge" HeaderText="Service Charge" />
                    <asp:BoundField DataField="PGCharges" HeaderText="Convenience Fee" />
                    <asp:TemplateField HeaderText="DETAIL">
                        <ItemTemplate>
                            <div class="tag">
                                <a href="#" class="gridViewToolTip">
                                    <img src="<%=ResolveUrl("~/Images/view_icon.gif")%>" border="0" /></a>
                                <div id="tooltip" style="display: none;">
                                    <div style="float: left;">
                                        <table width="100%" cellpadding="11" cellspacing="11" border="0">
                                            <tr>
                                                <td style="width: 110px; font-weight: bold;">Hotel Name:
                                                </td>
                                                <td style="width: 166px;">
                                                    <%#Eval("HotelName")%>  
                                                </td>
                                                <td style="width: 110px; font-weight: bold;">CheckIn Date:
                                                </td>
                                                <td style="width: 166px;">
                                                    <%#Eval("CheckIN")%>  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 110px; font-weight: bold;">Room Name:
                                                </td>
                                                <td style="width: 166px;">
                                                    <%#Eval("RoomName")%>  
                                                </td>

                                                <td style="width: 110px; font-weight: bold;">ChekOut Date:
                                                </td>
                                                <td style="width: 166px;">
                                                    <%#Eval("CheckOut")%>  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 110px; font-weight: bold;">Trip Type:
                                                </td>
                                                <td style="width: 166px;">
                                                    <%#Eval("TripType")%>  
                                                </td>
                                                <td style="width: 110px; font-weight: bold;">Booking Date:
                                                </td>
                                                <td style="width: 166px;">
                                                    <%#Eval("BookingDate")%>  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 110px; font-weight: bold;">Star Rating:
                                                </td>
                                                <td style="width: 166px;">
                                                    <%#Eval("StarRating")%> 
                                                </td>
                                                <td style="width: 110px; font-weight: bold;">Executive ID:
                                                </td>
                                                <td style="width: 166px;">
                                                    <%#Eval("ExecutiveID")%>  
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remark">
                        <ItemTemplate>
                            <div class="tag">
                                <a href="#" class="gridViewToolTip">
                                    <img src="<%=ResolveUrl("~/Images/view_icon.gif")%>" border="0" /></a>
                                <div id="tooltip" style="display: none;">
                                    <div style="float: left;">
                                        <table width="100%" cellpadding="11" cellspacing="11" border="0">
                                            <tr>
                                                <td style="width: 130px; font-weight: bold;">Request Remark:
                                                </td>
                                                <td style="width: 166px;">
                                                    <%#Eval("RequestRemark")%>  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 130px; font-weight: bold;">Request Date:
                                                </td>
                                                <td style="width: 166px;">
                                                    <%#Eval("RequestDate")%>  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 130px; font-weight: bold;">Accept Date:
                                                </td>
                                                <td style="width: 166px;">
                                                    <%#Eval("AcceptDate") %> 
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 130px; font-weight: bold;">Updated Remark:
                                                </td>
                                                <td style="width: 166px;">
                                                    <%#Eval("UpdateRemark")%>  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 130px; font-weight: bold;">Updated Date:
                                                </td>
                                                <td style="width: 166px;">
                                                    <%#Eval("UpdateDate") %>  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 130px; font-weight: bold;">Reject Remark:
                                                </td>
                                                <td style="width: 166px;">
                                                    <%#Eval("RejectRemark")%>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                 </asp:GridView>
                 </ContentTemplate>
                   </asp:UpdatePanel>
                                     </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

      <script type="text/javascript">
          var myDate = new Date();
          var currDate = (myDate.getDate()) + '-' + (myDate.getMonth() + 1) + '-' + myDate.getFullYear();
         // $("#From").val(currDate); $("#To").val(currDate);

          var UrlBase = '<%=ResolveUrl("~/") %>';
         var CheckindtPickerOptions = { numberOfMonths: 1, dateFormat: "dd-mm-yy", maxDate: "+1y", minDate: "-1y", showOtherMonths: true, selectOtherMonths: false };
         $("#Checkin").datepicker(CheckindtPickerOptions);
        //var CheckindtPickerOptions = {
        //    numberOfMonths: 1, dateFormat: "dd-mm-yy", maxDate: "+1y", minDate: "-1y", showOtherMonths: true, selectOtherMonths: false
        //};
        //$("#Checkin").val(currDate);
        //$("#Checkin").datepicker(CheckindtPickerOptions);
        //    $("#ctl00_ContentPlaceHolder1_Checkin").datepicker(CheckindtPickerOptions).datepicker("option", { onSelect: h.UpdateRoundTripMininumDate }).datepicker("setDate", FromDate.substr(0, 10));

        $(function () {
            InitializeToolTip();
        });
    </script>

</asp:Content>

