﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="false"
    CodeFile="DomDiscountMaster.aspx.vb" Inherits="DetailsPort_Admin_DomDiscountMaster" %>

<%@ Register Src="~/UserControl/Settings.ascx" TagPrefix="uc1" TagName="Settings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        function vali_number() {
            if ((event.keyCode >= 0 && event.keyCode < 46) || (event.keyCode > 46 && event.keyCode < 48) || (event.keyCode > 57 && event.keyCode < 128))
            { event.returnValue = false; }
            else
            { event.returnValue = true; }
        }

        function checkvld() {
            if (document.getElementById("ctl00_ContentPlaceHolder1_grade").value == "") {
                alert("Please Select Agent Type");
                return false;
            }
            else if ((document.getElementById("ctl00_ContentPlaceHolder1_grade").value != "") && (document.getElementById("ctl00_ContentPlaceHolder1_Airlag").value == "")) {
                alert("Please Select AirLine");
                return false;
            }
            else { }
        }
    </script>
  

 
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>

    <div class="row">
       <div class="col-md-12"  >
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Flight Setting > Domestic Discount Master</h3>
                    </div>
                    <div class="panel-body">
                        <asp:UpdatePanel ID="UP" runat="server">
                            <ContentTemplate>
                                <div class="row">

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Agent Type :</label>
                                            <asp:DropDownList CssClass="form-control" ID="grade" runat="server"></asp:DropDownList>
                                        </div>
                                    </div>




                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div id="tr_Dis" runat="server" class="large-5 medium-5 small-12 columns">
                                                <label for="exampleInputPassword1">Airline :</label>
                                                <asp:DropDownList ID="Airlag" CssClass="form-control" runat="server" AutoPostBack="true">
                                                    <asp:ListItem Value="">Select AirLine</asp:ListItem>
                                                    <asp:ListItem Value="AI">AirIndia</asp:ListItem>
                                                    <asp:ListItem Value="IC">Indian Airlines</asp:ListItem>
                                                    <asp:ListItem Value="9W">JetAirways</asp:ListItem>
                                                    <asp:ListItem Value="S2">JetLite</asp:ListItem>
                                                    <asp:ListItem Value="IT">KingFisher</asp:ListItem>
                                                    <asp:ListItem Value="IT4">KingFisher-Red</asp:ListItem>
                                                    <asp:ListItem Value="G8">GoAir</asp:ListItem>
                                                    <asp:ListItem Value="6E">IndiGo</asp:ListItem>
                                                    <asp:ListItem Value="SG">SpiceJet</asp:ListItem>
                                                    <asp:ListItem Value="9Z">MDLR</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Discount : Basic</label>
                                            <asp:TextBox CssClass="form-control" runat="server" ID="txtDis"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RFVMK" runat="server" ControlToValidate="txtDis" ErrorMessage="*"
                                                Display="dynamic"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">YQ</label><br />
                                            <asp:TextBox CssClass="form-control" runat="server" ID="txtDisYQ"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDisYQ" ErrorMessage="*"
                                                Display="dynamic"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>

                                        </div>

                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Basic+YQ</label><br />

                                            <asp:TextBox CssClass="form-control" runat="server" ID="txtDisBYQ"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDisBYQ" ErrorMessage="*"
                                                Display="dynamic"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">PLB  Discount : PLB Basic</label>
                                            <asp:TextBox CssClass="form-control" runat="server" ID="txtPlbBasic"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPlbBasic" ErrorMessage="*"
                                                Display="dynamic"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                                        </div>

                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">PLB YQ</label><br />
                                            <asp:TextBox CssClass="form-control" runat="server" ID="txtPlbYQ"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtPlbYQ" ErrorMessage="*"
                                                Display="dynamic"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">PLB Basic+YQ</label><br />
                                            <asp:TextBox CssClass="form-control" runat="server" ID="txtPlbBasicYQ"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtPlbBasicYQ" ErrorMessage="*"
                                                Display="dynamic"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Restrict RBD</label><br />
                                            <asp:TextBox CssClass="form-control" runat="server" ID="txtPlbRbd"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtPlbRbd" ErrorMessage="*"
                                                Display="dynamic"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                                        </div>
                                    </div>


                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Cash Back :</label>
                                            <asp:TextBox CssClass="form-control" runat="server" ID="txtcb"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtcb" ErrorMessage="*"
                                                Display="dynamic"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                                        </div>
                                    </div>


                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Class</label><br />
                                            <asp:DropDownList ID="DDL_PLBClass" runat="server" CssClass="form-control">
                                                 <asp:ListItem Value="All">--All--</asp:ListItem>
                            <asp:ListItem Value="C">Business</asp:ListItem>
                            <asp:ListItem Value="Y">Economy</asp:ListItem>
                            <asp:ListItem Value="F">First</asp:ListItem>
                            <asp:ListItem Value="W">Premium Economy</asp:ListItem>
                            <asp:ListItem Value="P">Premium First</asp:ListItem>

                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-4" style="display: none">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Discount :</label>
                                            <asp:TextBox ID="txtDdis" runat="server" CssClass="form-control" MaxLength="2"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <br />
                                        <div class="form-group">
                                            <asp:Button ID="UpdateAg" runat="server" Text="Update" CssClass="button buttonBlue" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4" style="display: none;">
                                        <div class="form-group">
                                            <asp:Button ID="UpdateDi" runat="server" Text="Update" CssClass="button buttonBlue" />
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="display: none;">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Distributor :</label>
                                            <asp:DropDownList ID="dist" runat="server" CssClass="form-control" AutoPostBack="true">
                                            </asp:DropDownList>

                                            <asp:DropDownList Visible="false" ID="Airlinedi" runat="server" CssClass="form-control" AutoPostBack="true">
                                                <asp:ListItem>Select AirLine</asp:ListItem>
                                                <asp:ListItem Value="AI">AirIndia</asp:ListItem>
                                                <asp:ListItem Value="IC">Indian Airlines</asp:ListItem>
                                                <asp:ListItem Value="9W">JetAirways</asp:ListItem>
                                                <asp:ListItem Value="S2">JetLite</asp:ListItem>
                                                <asp:ListItem Value="IT">KingFisher</asp:ListItem>
                                                <asp:ListItem Value="GOAIR">GoAir</asp:ListItem>
                                                <asp:ListItem Value="INDIGO">IndiGo</asp:ListItem>
                                                <asp:ListItem Value="SPICEJET">SpiceJet</asp:ListItem>
                                                <asp:ListItem Value="9Z">MDLR</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label style="display: none;" for="exampleInputPassword1">Tranasaction Fee :</label>
                                            <asp:RadioButton Visible="false" ID="yes" CssClass="form-control" Text="Y" Checked="True" GroupName="Fee" runat="server" />
                                            <asp:RadioButton Visible="false" ID="No" Text="N" GroupName="Fee" runat="server" />
                                        </div>

                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                        AllowSorting="True" CssClass="table" GridLines="None" Width="100%">
                                        <Columns>
                                            <asp:BoundField DataField="Grade" HeaderText="Discount Types" />
                                            <asp:BoundField DataField="Airline" HeaderText="Airline" />
                                            <asp:BoundField DataField="Dis" HeaderText="Basic" />
                                            <asp:BoundField DataField="Dis_YQ" HeaderText="YQ" />
                                            <asp:BoundField DataField="Dis_B_YQ" HeaderText="Basic+YQ" />
                                            <asp:BoundField DataField="CB" HeaderText="Cash Back" />
                                            <asp:BoundField DataField="PlbBasic" HeaderText="Plb&nbsp;Basic" />
                                            <asp:BoundField DataField="PlbYQ" HeaderText="Plb&nbsp;YQ" />
                                            <asp:BoundField DataField="PlbBYQ" HeaderText="Plb&nbsp;BYQ" />
                                            <asp:BoundField DataField="RBD" HeaderText="RBD" />
                                            <asp:BoundField DataField="Class" HeaderText="Class" />

                                            <%--  <asp:BoundField DataField="UpdatedBy" HeaderText="UPDATED BY" />
                                             <asp:BoundField DataField="UpdatedDate" HeaderText="UPDTED DATE" />--%>
                                        </Columns>
                                        <RowStyle CssClass="RowStyle" />
                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                        <PagerStyle CssClass="PagerStyle" />
                                        <SelectedRowStyle CssClass="SelectedRowStyle" />
                                        <HeaderStyle CssClass="HeaderStyle" />
                                        <EditRowStyle CssClass="EditRowStyle" />
                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                    </asp:GridView>
                                </div>

                            </ContentTemplate>
                        </asp:UpdatePanel>

                        <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UP">
                            <ProgressTemplate>
                                <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                </div>
                                <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                    Please Wait....<br />
                                    <br />
                                    <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                    <br />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>

                    </div>
                </div>

            </div>
        </div>
    </div>

</asp:Content>
