﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPageSignIn.master" CodeFile="StockistPanel.aspx.cs" Inherits="DetailsPort_Admin_StockiestPannel" %>

<asp:Content ID="content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  



    <div class="row">
        <div class="container-fluid" style="padding-right: 35px;">
            <div class="page-wrapperss">
                <div class="panel panel-primary">

                    <div class="panel-body">

                        <div class="row">
                       <div class="col-xs-2">
                                <div class="input-group">
                                   <span id="Span1" runat="server">
                                    <input type="text" id="txtStockistName" name="txtStockistName"
                                        autocomplete="off" placeholder="DISTRIBUTOR"
                                        class="form-control input-text full-width ui-autocomplete-input" role="textbox" aria-autocomplete="list" aria-haspopup="true" runat="server" enableviewstate="false" /></span>
                                
                                    <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="fa fa-black-tie"></span>
                                    </span>
                                    <input type="hidden" id="hidtxtStockistName" name="hidtxtStockistName" value="" runat="server" />
                                </div>
                            </div>
   
                            <div id="Div_ToStockist" class="col-xs-2" runat="server">
                                 <%--<label for="exampleInputPassword1">Change To  </label>--%>
                                <div class="input-group">
                                   
                                    <input type="text" id="TotxtStockistName" name="TotxtStockistName"
                                        placeholder="AGENCY NAME or ID" autocomplete="off" 
                                        class="input-text full-width ui-autocomplete-input" role="textbox" aria-autocomplete="list" aria-haspopup="true" runat="server" />

                                    <input type="hidden" id="TohidtxtStockistName" name="TohidtxtStockistName" value="" runat="server" />
                                </div>
                            </div>


                            <div id="Div_BtnSearch" class="col-xs-2" runat="server" >
                                <div class="input-group">
                                    <br />
                                    <asp:Button ID="BtnSearchStockist" runat="server" Text="Search" CssClass="btn btn-success" OnClick="BtnSearchStockist_Click" />
                                </div>

                            </div>
                            <div class="col-md-4">
                                <div class="form Group">
                                    <br />
                                    <asp:Button ID="ChangeAgentStatus" runat="server" Text="Transfer" CssClass="btn btn-success" OnClick="ChangeAgentStatus_Click" OnClientClick="Confirm()" />
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>

                        <div class="clear"></div>

                        <div class="clear"></div>
                        <div class="clear"></div>
                        <div class="clear"></div>
                        <div class="row" id="div_gridview" runat="server">
                            <div class="col-md-10">
                                <asp:GridView ID="gv1" runat="server" AutoGenerateColumns="false" CssClass="table">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Agency_Name">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox1_AGENCYNAME" runat="server" Text='<%# Bind("Agency_Name") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label1_AGENCYNAME" runat="server" Text='<%# Bind("Agency_Name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="AgencyId">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox_AgencyId" runat="server" Text='<%# Bind("AgencyId") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label_AgencyId" runat="server" Text='<%# Bind("AgencyId") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Agent_Type">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox_Agent_Type" runat="server" Text='<%# Bind("Agent_Type") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label_Agent_Type" runat="server" Text='<%# Bind("Agent_Type") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="User_Id">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox2_User_Id" runat="server" Text='<%# Bind("User_Id") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label2_User_Id" runat="server" Text='<%# Bind("User_Id") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Distr">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox3_Distr" runat="server" Text='<%# Bind("Distr") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label3_Distr" runat="server" Text='<%# Bind("Distr") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox4_Status" runat="server" Text='<%# Bind("Status") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label4_Status" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="DueAmount">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox5_DueAmount" runat="server" Text='<%# Bind("DueAmount") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label5_DueAmount" runat="server" Text='<%# Bind("DueAmount") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Crd_Limit">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox_Crd_Limit" runat="server" Text='<%# Bind("Crd_Limit") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label_Crd_Limit" runat="server" Text='<%# Bind("Crd_Limit") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Remark">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox6_Remark" runat="server" Text='<%# Bind("Remark") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label6_Remark" runat="server" Text='<%# Bind("Remark") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TransferToStokist">
                                            <EditItemTemplate>
                                                <asp:CheckBox ID="CheckBox1" runat="server" CssClass="chkSto" />
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBox1" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>

                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <%--<script src="<%=ResolveUrl("~/Scripts/jquery-1.7.1.min.js")%>" type="text/javascript"></script>--%>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
    <script language="javascript" type="text/javascript">
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Do you want to Transfer?")) {
                confirm_value.value = "Yes";

            }
            else {
                confirm_value.value = "No";


            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>

</asp:Content>
