﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DetailsPort_PrivilegePanel_RoleMaster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Submit_Click(object sender, EventArgs e)
    {
        {
            Pro bo = new Pro();

            string result = "";

            bo.U_role = Roletxt.Text;
            bo.Role_Type = RoleType.SelectedValue;


            Bal bl = new Bal();

            try
            {
                result = bl.insert(bo);
                if (result == "Insert")
                {
                    BindGridview();
                    Label1.InnerText = "Data successfully inserted in database";
                    Roletxt.Text = "";
                }
                else
                {

                    Label1.InnerText = "Data Already Exist";
                    Roletxt.Text = "";
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                bo = null;
                bl = null;
            }

        }
    }
    //private static DataTable GetData(string query)
    //{

    //    string constr = ConfigurationManager.ConnectionStrings["reg_connection"].ConnectionString;
    //    SqlConnection con = new SqlConnection(constr);
    //    con.Open();

    //    SqlCommand cmd = new SqlCommand();
    //    cmd.CommandText = query;

    //    SqlDataAdapter sda = new SqlDataAdapter();

    //    cmd.Connection = con;
    //    sda.SelectCommand = cmd;
    //    DataTable dt = new DataTable();

    //    sda.Fill(dt);
    //    con.Close();

    //    return dt;
    //}
    protected void BindGridview()
    {

        string constr = ConfigurationManager.ConnectionStrings["myAmdDB1"].ConnectionString;
        SqlConnection con = new SqlConnection(constr);
        con.Open();

        SqlCommand cmd = new SqlCommand("BindGVRole_PP");
        cmd.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter sda = new SqlDataAdapter();
        cmd.Connection = con;
        sda.SelectCommand = cmd;

        DataTable dt = new DataTable();
        sda.Fill(dt);
        GridView1.DataSource = dt;
        con.Close();

        RoleType.Items.Insert(0, new ListItem("--Select RoleType--", "0"));

        GridView1.DataBind();

    }

    protected void OnRowCancelingEdit(object sender, EventArgs e)
    {
        GridView1.EditIndex = -1;
        this.BindGridview();
    }

    protected void OnRowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        this.BindGridview();
    }

    protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int Roleid = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Values[0]);
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB1"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("DeleteSp_PP"))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Role_id", Roleid);
                cmd.Connection = con;
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }
        this.BindGridview();
    }


    protected void OnRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        GridViewRow row = GridView1.Rows[e.RowIndex];
        int Roleid = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Values[0]);

        string Role = (row.FindControl("txtRole") as TextBox).Text;
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB1"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("UpdateSp_PP"))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Role_ID", Roleid);
                cmd.Parameters.AddWithValue("@Role", Role);
                cmd.Connection = con;
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }
        GridView1.EditIndex = -1;
        this.BindGridview();
    }

}