﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DetailsPort_Finance_PGStatusByOorderId : System.Web.UI.Page
{
    PG.PaymentGateway obPg = new PG.PaymentGateway();
    protected void Page_Load(object sender, EventArgs e)
    {
        ((Label)Page.Master.FindControl("lblBC")).Text = "<a title='' class='tip-bottom' href=" + ResolveClientUrl("~/DetailsPort/PrivilegePanel/Splashboard.aspx") + " data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Account</a><a class='current' href='#'>Check PG Payment Status By Order Id</a>";
        
        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        //if (Session["User_Type"].ToString().ToUpper() != "ADMIN")
        //{
        //    Response.Redirect("~/Login.aspx");
        //}

        if (!string.IsNullOrEmpty( Convert.ToString(Session["UID"])))
        {
            trPGResponse.Visible = false;
            trPGResponse1.Visible = false;

            trAgentDetails.Visible = false;
            trAgentDetails1.Visible = false;

            trPaymentDetail.Visible = false;
            trPaymentDetail1.Visible = false;

            trRequest.Visible = false;
            trRequest1.Visible = false;

            trLedger.Visible = false;
            trLedger1.Visible = false;
        }
        else
        {
            Response.Redirect("~/Login.aspx");
        }
    }
    protected void btn_result_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txt_OrderId.Text))
        {
            GetDetails(txt_OrderId.Text);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Enter RefNo');", true);
            return;
        }
    }

    protected void BtnChangeStatus_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(HdnUserId.Value) && !string.IsNullOrEmpty(hdnOrderNo.Value))
        {
            string CreatedBy = Convert.ToString(Session["UID"]);
            string ipAddress = null;
            ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ipAddress) | ipAddress == null)
            {
                ipAddress = Request.ServerVariables["REMOTE_ADDR"];
            }
            string res = obPg.UpdatePaymentDetailsFromApiResponse(HdnUserId.Value, hdnOrderNo.Value, ipAddress, CreatedBy, false);

            GetDetails(hdnOrderNo.Value);
            if (res == "yes")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('updated successfully.');", true);
                return;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('try again.');", true);
                return;
            }
            //string AgentId, string OrderId, string IPAddress, string ActionBy, bool AddToWallet
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('try again');", true);
            return;
        }
    }
    protected void BtnAddAmountInwallet_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(HdnUserId.Value) && !string.IsNullOrEmpty(hdnOrderNo.Value) && !string.IsNullOrEmpty(hdnApiStatus.Value) && !string.IsNullOrEmpty(hdnUnmappedStatus.Value) && hdnApiStatus.Value.ToLower() == "success" && hdnUnmappedStatus.Value.ToLower() == "captured")
        {
            string CreatedBy = Convert.ToString(Session["UID"]);
            string ipAddress = null;
            ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ipAddress) | ipAddress == null)
            {
                ipAddress = Request.ServerVariables["REMOTE_ADDR"];
            }
            string res = "";
            res = obPg.UpdatePaymentDetailsFromApiResponse(HdnUserId.Value, hdnOrderNo.Value, ipAddress, CreatedBy, true);
            GetDetails(hdnOrderNo.Value);

            if(res=="yes")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('updated successfully.');", true);
                return;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('try again.');", true);
                return;
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('try again');", true);
            return;
        }

    }

    protected void GetDetails(string OrderId)
    {
        string APIStatus = string.Empty;
        string UnmappedStatus = string.Empty;
        double TotalAmountCredit = 0;
        string CreditLimitFlag = string.Empty;
        string PGStatus = string.Empty;

       
        string OldApiStatus = string.Empty;


        if (!string.IsNullOrEmpty(OrderId))
        {           
            try
            {
                string Response = obPg.GetPaymmentStatusResponsePayU(OrderId);
                DataSet ds = new DataSet();
                ds = obPg.GetPaymentDetailsByOrderId(OrderId);
                hdnOrderNo.Value = OrderId;
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        HdnUserId.Value = Convert.ToString(ds.Tables[0].Rows[0]["user_id"]);
                        tdTotalBAl.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["Crd_Limit"]);
                        tdAgentId.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["user_id"]) + "/" + Convert.ToString(ds.Tables[0].Rows[0]["AgencyId"]);
                        tdMobile.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["Mobile"]);

                        tdAgentCredit.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["AgentLimit"]);
                        tdAgnetName.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["Title"]) + " " + Convert.ToString(ds.Tables[0].Rows[0]["Fname"]) + " " + Convert.ToString(ds.Tables[0].Rows[0]["Lname"]);
                        tdEmail.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["Email"]);

                        tdDueAmount.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["DueAmount"]);
                        tdAgencyName.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["Agency_Name"]);
                        tdAddress.InnerText = Convert.ToString(ds.Tables[0].Rows[0]["Address"]);

                    }
                    if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                    {
                        tdOrderId.InnerText = Convert.ToString(ds.Tables[1].Rows[0]["TrackId"]);
                        tdServiceType.InnerText = Convert.ToString(ds.Tables[1].Rows[0]["ServiceType"]);
                        tdUserIdAndAgencyName.InnerText = Convert.ToString(ds.Tables[1].Rows[0]["AgentId"]) + "/" + Convert.ToString(ds.Tables[1].Rows[0]["AgencyName"]);
                        tdPGSatus.InnerText = Convert.ToString(ds.Tables[1].Rows[0]["Status"]) + "/" + Convert.ToString(ds.Tables[1].Rows[0]["UnmappedStatus"]);
                        tdCheckStatus.InnerText = Convert.ToString(ds.Tables[1].Rows[0]["UnmappedStatus"]);
                        PGStatus = Convert.ToString(ds.Tables[1].Rows[0]["Status"]);
                        OldApiStatus = Convert.ToString(ds.Tables[1].Rows[0]["ApiStatus"]);
                        HdnOldApiStatus.Value = OldApiStatus;
                        tdApiStatusData.InnerText = Convert.ToString(ds.Tables[1].Rows[0]["ApiStatus"]);
                        
                        tdOrignalAmount.InnerText = Convert.ToString(ds.Tables[1].Rows[0]["OriginalAmount"]);
                        tdTransCharge.InnerText = Convert.ToString(ds.Tables[1].Rows[0]["TotalCharges"]);
                        tdTotalAmount.InnerText = Convert.ToString(ds.Tables[1].Rows[0]["Amount"]);
                        tdBankRefNo.InnerText = Convert.ToString(ds.Tables[1].Rows[0]["BankRefNo"]);
                        tdPaymentMode.InnerText = Convert.ToString(ds.Tables[1].Rows[0]["PaymentMode"]);
                        tdErrorMsg.InnerText = Convert.ToString(ds.Tables[1].Rows[0]["ErrorText"]);
                        CreditLimitFlag = Convert.ToString(ds.Tables[1].Rows[0]["CreditLimitUpdate"]);


                    }
                    if (ds.Tables.Count > 2 && ds.Tables[2].Rows.Count > 0)
                    {
                        string TotalCreditAmount = Convert.ToString(ds.Tables[2].Rows[0]["TotalCredit"]);
                        if (!string.IsNullOrEmpty(TotalCreditAmount))
                        {
                            hdnTotalCredit.Value = TotalCreditAmount;
                            TotalAmountCredit = Convert.ToDouble(TotalCreditAmount);
                        }
                        else
                        {
                            hdnTotalCredit.Value = "0";
                            TotalAmountCredit = 0;
                        }
                    }
                    if (ds.Tables.Count > 3)
                    {
                        Grid1.DataSource = ds.Tables[3];
                        Grid1.DataBind();
                    }
                    if (ds.Tables.Count > 4)
                    {
                        GridView1.DataSource = ds.Tables[4];
                        GridView1.DataBind();
                    }
                }

                if (!string.IsNullOrEmpty(Response) && Response !="Not getting any response from PG.")
                {
                    #region Parse JSON RESPONS
                    Newtonsoft.Json.Linq.JObject account = Newtonsoft.Json.Linq.JObject.Parse(Response);
                    Details objDetails = new Details();
                    trPGResponse.Visible = true;
                    trPGResponse1.Visible = true;

                    trAgentDetails.Visible = true;
                    trAgentDetails1.Visible = true;

                    trPaymentDetail.Visible = true;
                    trPaymentDetail1.Visible = true;

                    trRequest.Visible = true;
                    trRequest1.Visible = true;
                    
                    trLedger.Visible = true;
                    trLedger1.Visible = true;

                    APIStatus = (string)account.SelectToken("transaction_details." + OrderId + ".status");
                    UnmappedStatus = (string)account.SelectToken("transaction_details." + OrderId + ".unmappedstatus");

                    tdApiStatus.InnerText = (string)account.SelectToken("transaction_details." + OrderId + ".status") + "/" + UnmappedStatus;
                    hdnApiStatus.Value = (string)account.SelectToken("transaction_details." + OrderId + ".status");
                    hdnUnmappedStatus.Value = (string)account.SelectToken("transaction_details." + OrderId + ".unmappedstatus");
                    tdApiResponse.InnerText = Response;
                    #endregion Parse JSON RESPONS
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Not getting response from PG');", true);
                }
                if (APIStatus.ToLower() == "success" && TotalAmountCredit < 1 && CreditLimitFlag.ToLower() == "false" && UnmappedStatus.ToLower() == "captured" && PGStatus.ToLower() != "success")
                {
                    BtnAddAmountInwallet.Visible = true;
                }
                else if (string.IsNullOrEmpty(OldApiStatus) && APIStatus.ToLower() == "success" && TotalAmountCredit < 1 && CreditLimitFlag.ToLower() == "false" && UnmappedStatus.ToLower() == "captured" && PGStatus.ToLower() == "success")
                {
                    BtnAddAmountInwallet.Visible = true;
                }

                else
                {
                    BtnAddAmountInwallet.Visible = false;
                }

                if (PGStatus.ToLower() == "success"  && (!string.IsNullOrEmpty(OldApiStatus)))
                {
                    BtnChangeStatus.Visible = false;
                }
                else
                {
                    BtnChangeStatus.Visible = true;
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + ex.Message + "');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Enter RefNo');", true);
            return;
        }
    }

}