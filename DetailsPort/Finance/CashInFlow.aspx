﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="false"
    CodeFile="CashInFlow.aspx.vb" Inherits="DetailsPort_Finance_CashInFlow" %>

<%@ Register Src="~/UserControl/AccountsControl.ascx" TagPrefix="uc1" TagName="Account" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   <%-- <link href="../../css/main2.css" rel="stylesheet" type="text/css" />--%>

    <script src="../../JS/JScript.js" type="text/javascript"></script>

    <script src="../../JS/lytebox.js" type="text/javascript"></script>

    <%--<link href="../../CSS/lytebox.css" rel="stylesheet" type="text/css" />--%>

    <%--<link href="../../CSS/style.css" rel="stylesheet" type="text/css" />--%>
    <link href="../../css/basic.css" rel="stylesheet" type="text/css" />
          <style type="text/css">
        .txtCalander {
            width: 100px;
            background-image: url(../../images/cal.gif);
            background-repeat: no-repeat;
            background-position: right;
            cursor: pointer;
            border: 1px #D6D6D6 solid;
            margin-bottom: 10px;
        }

        .form-control {
            margin-bottom: 20px;
        }
    </style>

    <script type='text/javascript'>
        function validate() {

            if (document.getElementById("ctl00_ContentPlaceHolder1_txt_TC").value == "") {
                alert('Please Fill TC');
                document.getElementById("ctl00_ContentPlaceHolder1_txt_TC").focus();
                return false;

            }
            if (document.getElementById("ctl00_ContentPlaceHolder1_txt_DBN").value == "") {
                alert('Please Fill DBN');
                document.getElementById("ctl00_ContentPlaceHolder1_txt_DBN").focus();
                return false;

            }
            if (document.getElementById("ctl00_ContentPlaceHolder1_txt_Rmk").value == "") {
                alert('Please Fill Rmk');
                document.getElementById("ctl00_ContentPlaceHolder1_txt_Rmk").focus();
                return false;

            }
            if (document.getElementById("ctl00_ContentPlaceHolder1_txt_Amt").value == "") {
                alert('Please Fill Amt');
                document.getElementById("ctl00_ContentPlaceHolder1_txt_Amt").focus();
                return false;

            }


        }
    </script>

    <div class="mtop80"></div>
    <%--<div class="container-fluid">--%>
        <%--<div class="large-2 medium-2 small-12 columns">
            <uc1:Account runat="server" ID="Settings" />
        </div>--%>

          <div class="col-md-12 container-fluid" style="padding-right: 35px">
            <div class="page-wrapperss">

                <div class="panel panel-primary">

                    <div class="panel-body">

                        <div class="row col-md-12">

<%--        <div class="container-fluid" style="padding-right: 35px;">

            <div class="row">
                
                    <div class="page-wrapperss">

                        <div class="panel panel-primary">

                            <div class="panel-body" style="overflow: scroll;">
                                <div class="large-14 medium-14 small-14 heading">
                                    <div class="page-wrapperss">

                                        <div class="panel panel-primary">

                                            <div class="panel-body">

                                                <div class="row col-md-12">--%>
                                                    <div class="col-md-2">
                                                        <div class="input-group">
                                                            <input type="text"  placeholder="From Date" name="From" id="From" class=" form-control input-text full-width" readonly="readonly" size="5"/>
                                                            <span class="input-group-addon" style="background: #49cced">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-2">
                                                        <div class="input-group">
                                                            <input type="text" placeholder="To Date" name="To" id="To" class="form-control input-text full-width" readonly="readonly" size="5" />
                                                            <span class="input-group-addon" style="background: #49cced">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>

                                                     <div class="col-md-2" id="td_ag" runat="server" style="margin-top: 37px;" >
                                                            <div class="input-group"  id="tr_AgencyName" runat="server" style="top:-39px">
                                                                <div id="tr_Agency" runat="server" class="input-group">
                                                                <input type="text" placeholder="Brand Name" id="txtAgencyName"  name="txtAgencyName" class="form-control input-text full-width" />
                                                                <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                                                                <span class="input-group-addon" style="background: #49cced">
                                                                    <span class="fa fa-black-tie"></span>
                                                                </span>
                                                            </div>
                                                        </div></div>

                                                    <div class="col-md-2" runat="server">
                                                        <div class="form-group" id="spn_Projects1" runat="server">

                                                            <asp:DropDownList ID="ddl_UploadType"  runat="server" CssClass="input-text full-width">
                                                                <%--<asp:ListItem Selected="True" Value="Select Type">Select Type</asp:ListItem>--%>
                                                                <asp:ListItem  value="" disabled selected style="display: none;">Upload Type</asp:ListItem>
                                                                <asp:ListItem Value="CA">Cash</asp:ListItem>
                                                                <asp:ListItem Value="CR">Credit</asp:ListItem>
                                                                <asp:ListItem Value="CC">Card</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                      <div id="Div2" class="col-xs-2" runat="server">
                                                        <div class="form-group" id="Div3" runat="server">

                                                            <asp:DropDownList ID="ddlInflowtype" runat="server" AutoPostBack="True" CssClass="input-text full-width">
                                                                <%--<asp:ListItem Selected="True" Value="" disabled selected style="display: none;">Cash Flow Type</asp:ListItem>--%>
                                                                <asp:ListItem  value="" disabled selected style="display: none;">Cash Flow Type</asp:ListItem>
                                                               <asp:ListItem Value="InFlow">InFlow</asp:ListItem>
                                                                    <asp:ListItem Value="OutFlow">OutFlow</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                       <div class="col-md-1">
                                                    <table>
                                                      <tr  id="tr_UploadType" runat="server">
                                                      <td class="form-group">
                                                         <%-- <label class="control-label col-sm-4" for="email3">Agency Type: </label>--%>

                                                                <asp:RadioButtonList ID="RBL_Type" runat="server" AutoPostBack="True" RepeatDirection="Horizontal" RepeatColumns="2" cellspacing="4" CellPadding="4">
                       
                                                                    <asp:ListItem Value="CA" >Cash</asp:ListItem>
                                                                    
                                                                    <asp:ListItem Value="CR">Credit</asp:ListItem>
                                                                </asp:RadioButtonList>

                                                            </td>
                                                        </tr>
                                                    </table>
                                                        </div>
                                                      <div class="col-md-1" >
                                                        <div class="form-group">
                                                            <asp:Button ID="btn_search" runat="server" Text="Go" CssClass="btn btn-success" />
                                                     

                                                        </div>

                                                    </div>
                            <div class="col-md-1" style="margin-left: 99px">
                                                        <div class="form-group">
     <asp:Button ID="btn_Export" runat="server" Text="Export" OnClientClick="return Validation()"
                                                                CssClass="btn btn-success" />
                                                       </div>

                                                    </div>




                                                    <%-- <tr id="tr1" runat="server">
                                                            <td width="90" valign="top">Payment Type:
                                                            </td>
                                                            <td width="290" valign="top">

                                                                <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="True" RepeatDirection="Horizontal"
                                                                    Width="290px" CellPadding="4" CellSpacing="4">
                                                                    <asp:ListItem Value="CA" Selected="True">&nbsp;Cash</asp:ListItem>
                                                                    <asp:ListItem Value="CR">&nbsp;Credit</asp:ListItem>
                                                                </asp:RadioButtonList>

                                                            </td>
                                                        </tr>
--%>
                                                   <div style="clear:both">&nbsp;</div>

                                                    <div id="tr" class="row col-md-4" runat="server" Visible="false">    <!--hidden field-->
                                                        <div class="form-group" id="tr_SearchType" runat="server">
                                                           <div>
                                                            <asp:RadioButton ID="RB_Agent" runat="server" Checked="true" GroupName="Trip" onclick="Show(this)"
                                                                Text="Agent" runat="server" RepeatDirection="Horizontal" RepeatColumns="2" cellspacing="4" CellPadding="4" />
                                                            </div>

                                                               <div>
                                                            <asp:RadioButton ID="RB_Distr" runat="server" GroupName="Trip" onclick="Hide(this)"
                                                                Text="Own" />
                                                                     </div>
                                                           
                                                        </div>
                                                    </div>
                                                   <%-- <div class="Container">
                                                         <label class="radio-inline">
                                                             <input type="radio" name="optradio">cash
                                                    </label>
                                                         <label class="radio-inline"  >
                                                             <input type="radio" name="optradio" id="RBL_Type"  AutoPostBack="True" RepeatDirection="Horizontal">Credit
                                                  </label>
                                                    </div>--%>
                                                 

                                                    <div class="row">
                                                   
                                                        </div>

                                                    
                                                    <%-- <td width="90" valign="top">Upload Type:
                                                        </td>
                                                        <td width="290" valign="top">
                                                            <asp:DropDownList ID="ddl_UploadType" runat="server" Width="290px" CssClass="input-text full-width">
                                                                <asp:ListItem Selected="True" Value="Select Type">Select Type</asp:ListItem>
                                                                <asp:ListItem Value="CA">Cash</asp:ListItem>
                                                                <asp:ListItem Value="CR">Credit</asp:ListItem>
                                                                <asp:ListItem Value="CC">Card</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>--%>

                                                   
                                                    <%-- <tr>
                                                        <td colspan="2" style="padding-top: 8px">
                                                            <asp:Button ID="btn_search" runat="server" Text="Go" CssClass="btn btn-success"  Width="100px"/>
                                                            &nbsp;<asp:Button ID="btn_Export" runat="server" Text="Export" OnClientClick="return Validation()"
                                                                CssClass="btn btn-success" Width="100px" />
                                                        </td>
                                                    </tr>--%>









                                                    


                                                    <%--                                                    <tr>
                                                        <td colspan="2" id="tr_SearchType" runat="server" visible="false">
                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td width="90" valign="top">Search Type
                                                                    </td>
                                                                    <td style="width: 290px">
                                                                        <asp:RadioButton ID="RB_Agent" runat="server" Checked="true" GroupName="Trip" onclick="Show(this)"
                                                                            Text="Agent" />
                                                                        &nbsp;&nbsp;&nbsp;
                                                            <asp:RadioButton ID="RB_Distr" runat="server" GroupName="Trip" onclick="Hide(this)"
                                                                Text="Own" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>--%>

                                                    <%-- <tr id="Div2" runat="server">
                                                            <td width="90" valign="top">Payment Type:
                                                            </td>
                                                            <td width="290" valign="top">--%>


                                                  








                                                  <%--  <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td valign="top" width="90">Cash Flow Type:
                                                            </td>
                                                            <td width="290" valign="top" style="margin-bottom: 20px;">
                                                                <asp:DropDownList ID="ddlInflowtype" runat="server" AutoPostBack="True" Width="290px" CssClass="input-text full-width">
                                                                    <asp:ListItem Value="InFlow">InFlow</asp:ListItem>
                                                                    <asp:ListItem Value="OutFlow">OutFlow</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>--%>
                                                      
                                                        <tr id="tr_UploadCategory" runat="server" >
                                                            <td width="90" valign="top">Brand Category:
                                                            </td>
                                                            <td width="290" valign="top">
                                                                <asp:DropDownList ID="ddl_Category" runat="server" Width="290px" CssClass="input-text full-width">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>



                                                       
                                                        <%--<tr>
                                                        <td colspan="2">
                                                            <table width="100%" id="td_ag" border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td id="tr_AgencyName" runat="server" width="90">Brand Name:
                                                                    </td>
                                                                    <td width="290" valign="top" id="tr_Agency" runat="server">
                                                                        <input type="text" id="txtAgencyName" name="txtAgencyName" style="width: 290px"
                                                                            class="fa fa-black-tie"/>
                                                                        <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>--%>
                                                        <%--<tr>
                                                        <td colspan="2" style="padding-top: 8px">
                                                            <asp:Button ID="btn_search" runat="server" Text="Search" CssClass="btn btn-success"  Width="100px"/>
                                                            &nbsp;<asp:Button ID="btn_Export" runat="server" Text="Export" OnClientClick="return Validation()"
                                                                CssClass="btn btn-success" Width="100px" />
                                                        </td>
                                                    </tr>--%>
                                                    

                                    <div class="clear"></div>

                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left">
                                                                <asp:Label CssClass="pnrdtls" ID="lbl_amount" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:UpdatePanel ID="UP" runat="server">
                                                                    <ContentTemplate>
                                                                        <table width="90%">
                                                                            <tr>
                                                                                <td id="td_SalesRmk" runat="server" visible="false" align="center">    <!--hidden field-->
                                                                                    <div id="basic-modal-content3" style="padding: 10px; overflow: auto; width: 800px; border: thin solid #161946;">
                                                                                        <table width="100%">
                                                                                            <tr>
                                                                                                <td class="h2" style="font-size: 14px; padding-bottom: 5px; font-family: arial, Helvetica, sans-serif; color: #161946;"
                                                                                                    align="center" colspan="6">Update Deposite Details
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="left" width="130px" height="30px">TransactionID/Cheque No
                                                                                                </td>
                                                                                                <td align="left" width="140px">
                                                                                                    <asp:TextBox ID="txt_TC" runat="server" Width="135px"></asp:TextBox>
                                                                                                </td>
                                                                                                <td align="left" width="120px">&nbsp;&nbsp;&nbsp; Deposite Bank Name
                                                                                                </td>
                                                                                                <td align="left" width="155px">
                                                                                                    <asp:TextBox ID="txt_DBN" runat="server" Width="150px"></asp:TextBox>
                                                                                                </td>
                                                                                                <td width="80px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Amount
                                                                                                </td>
                                                                                                <td align="left">
                                                                                                    <asp:TextBox ID="txt_Amt" runat="server" Width="90px"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="left" style="padding-top: 5px">Remark
                                                                                                </td>
                                                                                                <td align="left" colspan="5">
                                                                                                    <asp:TextBox ID="txt_Rmk" runat="server" Height="50px" TextMode="MultiLine" Width="500px"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="right" style="padding-right: 5px; padding-top: 5px;" valign="top">
                                                                                                    <asp:CheckBox ID="chk_status" runat="server" />
                                                                                                </td>
                                                                                                <td align="left" style="padding-top: 5px" colspan="3">Rest of the amount should be debited from portal balance
                                                        <br />
                                                                                                    &nbsp;<asp:Label ID="lbl_msg" runat="server" ForeColor="#FF3300" Font-Bold="True"></asp:Label>
                                                                                                </td>
                                                                                                <td colspan="2" align="left">
                                                                                                    <asp:Button ID="btn_Submit" runat="server" Text="Update" OnClientClick="return validate();"
                                                                                                        CssClass="btn btn-success" />
                                                                                                    &nbsp;<asp:Button ID="btn_cancel" runat="server" Text="Cancel" CssClass="btn btn-danger" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <asp:GridView ID="grd_CashInflow" runat="server" AutoGenerateColumns="false" DataKeyNames="AccID"
                                                                                        CssClass="table" AllowPaging="True" PageSize="30" Width="100%" style="text-transform:uppercase;">
                                                                                        <Columns>
                                                                                            <asp:TemplateField HeaderText="SL&nbsp;No.">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lbl_counter" runat="server" Text='<%#Eval("Counter") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Invoice&nbsp;No.">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lbl_Invoice" runat="server" Text='<%#Eval("InvoiceNo") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="AgencyID">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lbl_agentid" runat="server" Text='<%#Eval("AgencyId")%>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="User&nbsp;ID">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lbl_agentid" runat="server" Text='<%#Eval("AgentID") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Agency&nbsp;Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lbl_agencyname" runat="server" Text='<%#Eval("AgencyName") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Amount">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lbl_amt" runat="server" Text='<%#Eval("Amount") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Upload&nbsp;Type">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lbl_type" runat="server" Text='<%#Eval("UploadType") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                                <EditItemTemplate>
                                                                                                    <asp:TextBox ID="txt_updatetype" runat="server" Text='<%#Eval("UploadType") %>'></asp:TextBox>
                                                                                                </EditItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Remark&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lbl_remark" runat="server" Text='<%#Eval("Remark") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="YtrRcptNo">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lbl_YtrRcptNo" runat="server" Text='<%#Eval("YtrRcptNo") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                                <EditItemTemplate>
                                                                                                    <asp:TextBox ID="txt_YtrRcptNo" runat="server" Text='<%#Eval("YtrRcptNo") %>'></asp:TextBox>
                                                                                                </EditItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Updated&nbsp;Remark">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lbl_updatedremark" runat="server" Text='<%#Eval("UpdatedRemark") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                                <EditItemTemplate>
                                                                                                    <asp:TextBox ID="txt_updateremark" runat="server" Text='<%#Eval("UpdatedRemark") %>'></asp:TextBox>
                                                                                                </EditItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Created&nbsp;Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lbl_Cdate" runat="server" Text='<%#Eval("CreatedDate") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Updated&nbsp;Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lbl_updateddate" runat="server" Text='<%#Eval("UpdatedDate") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="View&nbsp;Adjustment">
                                                                                                <ItemTemplate>
                                                                                                    <asp:LinkButton ID="lnk_adjdtl" runat="server">
                                                    <a href='UploadAdjustment.aspx?Counter=<%#Eval("Counter")%>&Type=View'
                                                        rel="lyteframe" rev="width: 900px; height: 300px; overflow:hidden;" target="_blank"
                                                        style="font-family: Arial, Helvetica, sans-serif; font-size: 12px;
                                                        color: #161946"> Details
                                                        </a></asp:LinkButton>
                                                                                                    <%--   <asp:Button ID="btn_edit" runat="server" Text="Edit" Font-Bold="true" CommandName="Edit" />--%>
                                                                                                </ItemTemplate>
                                                                                                <%--<EditItemTemplate>
                                                        <asp:Button ID="btn_update" runat="server" Text="Update" CommandName="Update" Font-Bold="true" />
                                                        <asp:Button ID="btn_cancel" runat="server" Text="Cancel" CommandName="Cancel" Font-Bold="true" />
                                                    </EditItemTemplate>--%>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Edit/Update">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Button ID="btn_edit" runat="server" Text="Edit" Font-Bold="true" CommandName="Edit" />
                                                                                                </ItemTemplate>
                                                                                                <EditItemTemplate>
                                                                                                    <asp:Button ID="btn_update" runat="server" Text="Update" CommandName="Update" Font-Bold="true" />
                                                                                                    <asp:Button ID="btn_cancel" runat="server" Text="Cancel" CommandName="Cancel" Font-Bold="true" />
                                                                                                </EditItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Adjustment">
                                                                                                <ItemTemplate>
                                                                                                    <%--<asp:LinkButton ID="lnk_adj" runat="server">
                                                    <a href='UploadAdjustment.aspx?Counter=<%#Eval("Counter")%>&Type=Insert'
                                                        rel="lyteframe" rev="width: 900px; height: 300px; overflow:hidden;" target="_blank"
                                                        style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; 
                                                        color: #161946">  AdjustAmount
                                                        </a></asp:LinkButton>--%>

                                                                                                    <%--   <asp:Button ID="btn_edit" runat="server" Text="Edit" Font-Bold="true" CommandName="Edit" />--%>
                                                                                                </ItemTemplate>
                                                                                                <%--<EditItemTemplate>
                                                        <asp:Button ID="btn_update" runat="server" Text="Update" CommandName="Update" Font-Bold="true" />
                                                        <asp:Button ID="btn_cancel" runat="server" Text="Cancel" CommandName="Cancel" Font-Bold="true" />
                                                    </EditItemTemplate>--%>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Type">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lbl_Agent_Type" runat="server" Text='<%#Eval("Agent_Type") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="SalesExecID">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lbl_SalesExecID" runat="server" Text='<%#Eval("SalesExecID") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Edit Details" Visible="false">
                                                                                                <ItemTemplate>
                                                                                                    <asp:LinkButton ID="lnk_EditPayment" runat="server" CommandName="EditDetail" CommandArgument='<%#Eval("Counter") %>'
                                                                                                        Font-Bold="True" ForeColor="#161946" Font-Size="10px">Update&nbsp;Payment&nbsp;Details</asp:LinkButton>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="View">
                                                                                                <ItemTemplate>
                                                                                                    <asp:LinkButton ID="lnk_View" runat="server" Visible="false" CommandName="View" CommandArgument='<%#Eval("Counter") %>'
                                                                                                        Font-Bold="True" ForeColor="#161946" Font-Size="10px">View&nbsp;Payment&nbsp;Details</asp:LinkButton>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="" Visible="false">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lbl_SalesUpDate" runat="server" Text='<%#Eval("UpdatedDateSales") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                        <RowStyle CssClass="RowStyle" />
                                                                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                                                        <PagerStyle CssClass="PagerStyle" />
                                                                                        <SelectedRowStyle CssClass="SelectedRowStyle" />
                                                                                        <HeaderStyle CssClass="HeaderStyle" />
                                                                                        <EditRowStyle CssClass="EditRowStyle" />
                                                                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                                                                    </asp:GridView>
                                                                                    &nbsp;
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="top" align="left">
                                                                                    <div id="basic-modal-content" style="padding: 20px; overflow: auto; width: 100%;">
                                                                                        <table width="100%" border="0" cellpadding="10" cellspacing="10">
                                                                                            <tr>
                                                                                                <td colspan="4" style="font-family: arial, Helvetica, sans-serif; font-size: 14px;" align="left">
                                                                                                    <p class="bld">(Executive Updated Upload Details)</p>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="left" width="180px" style="">TransactionID/ChequeNo :
                                                                                                </td>
                                                                                                <td id="td_TC" runat="server" width="170px"></td>
                                                                                                <td align="left" width="130px" style="">Deposite Bank Name :
                                                                                                </td>
                                                                                                <td id="td_DBN" runat="server"></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="left" style="">Amount :
                                                                                                </td>
                                                                                                <td id="td_Amt" runat="server"></td>
                                                                                                <td align="left" style="">Updated Date :
                                                                                                </td>
                                                                                                <td id="td_UD" runat="server"></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="left" style="">Debit Portal Balance :
                                                                                                </td>
                                                                                                <td id="td_DPB" runat="server" class="style1"></td>
                                                                                                <td align="left" style="">Remark :
                                                                                                </td>
                                                                                                <td id="td_Rmk" runat="server" align="left" class="style1"></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                                <%--<asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UP">
                                <ProgressTemplate>
                                    <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden;
                                        padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5;
                                        z-index: 1000;">
                                    </div>
                                    <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center;
                                        z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px;
                                        font-weight: bold; color: #000000">
                                        Please Wait....<br />
                                        <br />
                                        <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                        <br />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="clear"></div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                
                    <script type="text/javascript">
                        var UrlBase = '<%=ResolveUrl("~/") %>';
                    </script>

                    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

                    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

                    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

                    <script src="../../Utility/JS/jquery.simplemodal.js" type="text/javascript"></script>

                    <script type="text/javascript" src="<%=ResolveUrl("~/JS/Distributor.js") %>"></script>

                    <script type='text/javascript'>

                        function openDialog() {

                            $(function () {
                                //                alert('hi');
                                $('#basic-modal-content').modal();
                                return false;

                            });




                        }

                    </script>
</asp:Content>
