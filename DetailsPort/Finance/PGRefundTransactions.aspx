﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="PGRefundTransactions.aspx.cs" Inherits="DetailsPort_Finance_PGRefundTransactions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <script type="text/javascript" src="<%=ResolveUrl("../../Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("../../Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>


    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .overfl {
            overflow-y: scroll;
        }
    </style>
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-10">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">PG Refund Transaction Report</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">From Date</label>
                                    <input type="text" name="From" id="From" class="form-control" readonly="readonly" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="exampleInputPassword1">To Date</label>
                                <input type="text" name="To" id="To" class="form-control" readonly="readonly" />
                            </div>
                         <%--   <div class="col-md-4">
                                <label for="exampleInputPassword1">AgencyName</label>
                                <asp:TextBox ID="Txt_AgencyName" class="form-control  AgencyName" runat="server"></asp:TextBox>

                            </div>--%>

                             <div class="col-md-4">
                                <div class="form-group" id="td_Agency" runat="server">
                                    <label for="exampleInputPassword1">Agency Name</label>
                                    <input type="text" id="txtAgencyName" class="form-control" name="txtAgencyName" onfocus="focusObj(this);"
                                        onblur="blurObj(this);" defvalue="" autocomplete="off"  value="" />
                                    <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value=""  />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Order Id</label>
                                    <asp:TextBox ID="txt_OrderId" class="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Status</label>
                                    <asp:DropDownList ID="drpPaymentStatus" runat="server" class="form-control">
                                        <asp:ListItem Value="A">All</asp:ListItem>
                                        <asp:ListItem Value="Refunded">Refunded</asp:ListItem>
                                        <asp:ListItem Value="Failure">Failed</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:Button ID="btn_result" runat="server" Text="Search Result" CssClass="button buttonBlue" OnClick="btn_result_Click" />
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:Button ID="btn_export" runat="server" Text="Export" CssClass="button buttonBlue" OnClick="btn_export_Click" />
                                </div>
                            </div>
                        </div>
                        <%--<div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:Label ID="lbl_Norecord" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>--%>
                        <div class="row" id="GVID" style="background-color: #fff; overflow-y: scroll; overflow-x: scroll; max-height: 500px;" runat="server">
                            <div class="col-md-28">

                                <asp:GridView ID="GridHoldPNRAccept" runat="server" AllowPaging="False" AllowSorting="True"
                                    AutoGenerateColumns="False" CssClass="table" GridLines="None">
                                    <Columns>
                                        <asp:TemplateField HeaderText="OrderId">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_OrderId" runat="server" Text='<%#Eval("OrderId") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="AgentId">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_AgentID" runat="server" Text='<%#Eval("AgentId") %>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="ReferenceNo">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Sector" runat="server" Text='<%#Eval("ReferenceNO") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="RefundReferenceNo">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Status" runat="server" Text='<%#Eval("RefundRefNo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="AgencyName">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Duration" runat="server" Text='<%#Eval("AgencyName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>



                                        <asp:TemplateField HeaderText="RefundAmount">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_TripType" runat="server" Text='<%#Eval("RefundAmount") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="RefundRemark">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Trip" runat="server" Text='<%#Eval("RefundRemark") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="RefundStatus">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_TourCode" runat="server" Text='<%#Eval("RefundStatus") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="RefundedBy">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_TotalBookingCost" runat="server" Text='<%#Eval("RefundedBy") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="RefundedDate">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_TotalAfterDis" runat="server" Text='<%#Eval("RefundedDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <%--  <asp:TemplateField ControlStyle-Width="100px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkSubmit" runat="server" Visible="false"
                                                    CommandArgument='<%# Eval("HeaderId") %>' CommandName="UpdateHold"><img src="../../Images/Submit.png" alt="Ok" /></asp:LinkButton><br />
                                                <asp:LinkButton ID="lnkHides" runat="server" Visible="false"
                                                    CommandName="RejectHold" CommandArgument='<%# Eval("HeaderId") %>'><img src="../../Images/Cancel.png" alt="Cancel" /></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                    </Columns>
                                    <RowStyle CssClass="RowStyle" />
                                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                    <PagerStyle CssClass="PagerStyle" />
                                    <SelectedRowStyle CssClass="SelectedRowStyle" />
                                    <HeaderStyle CssClass="HeaderStyle" Height="50px" />
                                    <EditRowStyle CssClass="EditRowStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

   <%-- <script type="text/javascript">
     
        var UrlBase = location.protocol + "//" + location.host + "/";
    </script>--%>

       <script type="text/javascript">
           var UrlBase = '<%=ResolveUrl("~/") %>';
                </script>

     <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
 
</asp:Content>
