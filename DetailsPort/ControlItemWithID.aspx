﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="ControlItemWithID.aspx.cs" Inherits="ControlItemWithID" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }

        .auto-style2 {
            height: 40px;
        }

        .auto-style3 {
            height: 22px;
        }
    </style>

     <!--TextBox-Validation-->
   <%-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"></link>--%>
        <script src="https://code.jquery.com/jquery-1.12.4.js">
        </script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js">
        </script>



    <script type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if ((charCode >= 48 && charCode <= 57) || (charCode == 8) || (charCode == 46)) {
                return true;
            }
            else {
                return false;
            }
        }
        function MyFunc(strmsg) {
            switch (strmsg) {
                case 1: {
                    alert("Deleted successfully!!");
                }
                    break;
                case 2: {
                    alert("Updated successfully!!");
                }
                    break;
                case 3: {
                    alert("Sorry enable to Update or delete the record please contact with admin!!");
                }
                    break;
            }
        }
    </script>


   <%-- <script type="text/javascript">
        function ValidateFunction() {
            if (document.getElementById("<%=txt_title.ClientID%>").value == "") {
                alert('Title can not be blank');
                document.getElementById("<%=txt_title.ClientID%>").focus();
                return false;
            }
            if (document.getElementById("<%=txt_subtitle.ClientID%>").value == "") {
                alert('Subtitle can not be blank');
                document.getElementById("<%=txt_subtitle.ClientID%>").focus();
                return false;
            }
            if (document.getElementById("<%=txt_price.ClientID%>").value == "") {
                alert('Price can not be blank');
                document.getElementById("<%=txt_price.ClientID%>").focus();
                return false;
            }
            if (document.getElementById("<%=txt_linkedurl.ClientID%>").value == "") {
                alert('LinkedURL time can not be blank');
                document.getElementById("<%=txt_linkedurl.ClientID%>").focus();
                return false;
            }

            var uploadcontrol = document.getElementById('<%=fu_images.ClientID%>').value;
            var reg = /^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.jpeg|.JPEG|.gif|.GIF|.png|.PNG|.jpg|.JPG)$/;
            if (uploadcontrol.length > 0) {
                if (reg.test(uploadcontrol)) {
                    return true;
                }
                else {
                    alert('please select .JPEG,.GIF,.PNG files only!!');
                    return false;
                }
            }
        }
    </script>--%>


    <script type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode >= 48 && charCode <= 57 || charCode == 08 || charCode == 46) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>


    <div class="row">
        <div class="col-md-2"></div>

        <div class="container-fluid" style="padding-right: 35px;">
            <div class="page-wrapperss">

                <div class="panel panel-primary">

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-2" style="width:176px" >
                                <div class="form-group">

                                   <%-- <label for="exampleInputEmail1"></label>--%>
                                    <asp:DropDownList ID="ddl_controlID" runat="server" AppendDataBoundItems="true" CssClass="input-text full-width" AutoPostBack="true" OnSelectedIndexChanged="Role_SelectedIndexChanged" required="">
                                        <asp:ListItem  value="" disabled selected style="display: none;">SELECT ITEM</asp:ListItem>
                                    </asp:DropDownList>
                                    <div class="help-block with-errors"></div>

                                </div>
                            </div>


                            <div class="col-xs-2" style="width:176px" >
                                <div class="input-group">

                                    <%--<label for="exampleInputEmail1"></label>--%>
                                    <asp:TextBox ID="txt_title" runat="server" placeholder="TITLE " Style="margin-bottom: 0px" Class="form-control input-text full-width" required=""></asp:TextBox>
                                    <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="fa fa-object-group"></span>
                                    </span>
                                     <div class="help-block with-errors"></div>
                                </div>
                            </div>

                            <div class="col-xs-2" style="width:176px" >
                                <div class="input-group">

                                    <%--<label for="exampleInputEmail1"></label>--%>
                                    <asp:TextBox ID="txt_subtitle" runat="server" placeholder="SUBTITLE" Class="form-control input-text full-width" required=""></asp:TextBox>
                                    <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="fa fa-object-group"></span>
                                    </span>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>




                            <div class="col-xs-2" style="width:176px" >
                                <div class="input-group">

                                  <%--  <label for="exampleInputEmail1"></label>--%>
                                    <asp:TextBox ID="txt_price" placeholder="PRICE" onkeypress="return isNumberKey(event)" MaxLength="6" runat="server" Class="form-control input-text full-width" required=""></asp:TextBox>
                                    <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="fa  fa-inr"></span>
                                    </span>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>


                           

                            <div class="col-xs-2" style="width:176px" >
                                <div class="input-group">


                                   <%-- <label for="exampleInputEmail1"></label>--%>
                                    <asp:TextBox ID="txt_linkedurl" placeholder="LINKED URL" runat="server" Class="form-control input-text full-width" required=""></asp:TextBox>
                                    <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="fa  fa-link"></span>
                                    </span>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                             <div class="col-xs-2" style="width:176px;margin-top:-18px" >
                                <div class="input-group">
                                    <br />
                                    <label for="exampleInputEmail1"></label>

                                    <asp:FileUpload CssClass="upload" ID="fu_images" runat="server" required="" style="margin-top: -5px;" />
                                   
                                       </div>

                                    
                                </div>
                                  <div class="col-xs-2" style="margin-left: 19px;">
                                <div class="input-group">

                                    <asp:Button ID="btn_submit" runat="server" OnClick="btn_submit_Click" OnClientClick="return ValidateFunction();" Text="Submit" CssClass="btn btn-success" />

                                </div>
                            

                        </div>
                         
                        </div>

                        

                           
                        <%--    <table class="auto-style1">
                                        <tr>
                                            <td class="auto-style2">Control ID :<asp:DropDownList ID="ddl_controlID" runat="server" Height="25px"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td>Item Title :<asp:TextBox ID="txt_title" runat="server" Style="margin-bottom: 0px"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>Item Subtitle :<asp:TextBox ID="txt_subtitle" runat="server"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style3">Price :<asp:TextBox ID="txt_price" onkeypress="return isNumberKey(event)" MaxLength="6" runat="server"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style3">Image URL :<asp:FileUpload ID="fu_images" runat="server" /></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style3">Linked URL :<asp:TextBox ID="txt_linkedurl" runat="server"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btn_submit" runat="server" OnClick="btn_submit_Click" OnClientClick="return ValidateFunction();" Text="Submit" /></td>
                                        </tr>
                                    </table>--%>

                       <div>&nbsp;</div>
                        <div class="row" id="divReport" style="background-color: #fff; overflow-y: scroll; overflow-x: scroll; max-height: 500px;" runat="server">
                            <div class="col-md-12">
                                <asp:GridView ID="gv1" runat="server" AutoGenerateColumns="false" AllowSorting="True"
                                    CssClass="table" GridLines="None" border="1"
                                    OnRowEditing="gv1_RowEditing"
                                    OnRowUpdating="gv1_RowUpdating"
                                    OnRowCancelingEdit="gv1_RowCancelingEdit"
                                    OnRowDeleting="gv1_RowDeleting"
                                    OnPageIndexChanging="gv1_PageIndexChanging" PageSize="30" AllowPaging="true">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Control Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_ControlID" runat="server" Text='<%# Eval("ControlName")%>'></asp:Label>
                                                <asp:Label ID="lbl_ItemID" runat="server" Visible="false" Text='<%# Eval("ItemID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Item Title">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_title" runat="server" Text='<%# Eval("ItemTitle")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txt_title" runat="server" Text='<%# Eval("ItemTitle")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Item SubTitle">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_subtitle" runat="server" Text='<%# Eval("ItemSubTitle")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txt_subtitle" runat="server" Text='<%# Eval("ItemSubTitle")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Price">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_price" runat="server" Text='<%# Eval("Price")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txt_price" runat="server" onkeypress="return isNumberKey(this);" Text='<%# Eval("Price")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Linked URL">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_linked" runat="server" Text='<%# Eval("LinkedURL")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txt_linked" runat="server" Text='<%# Eval("LinkedURL")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderStyle CssClass="hdrow" />
                                            <HeaderTemplate>
                                                <asp:Label ID="hlblimg" runat="server" Text="Image"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Image ID="img1" runat="server" ImageUrl='<%# Eval("ImageURL") %>'
                                                    Height="100px" Width="100px" />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:FileUpload ID="fu1" runat="server" />
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderStyle CssClass="hdrow" />
                                            <ItemTemplate>
                                                <asp:Button ID="btnedit" runat="server" Text="Edit" CommandName="Edit" />
                                                <asp:Button ID="btndelete" runat="server" Text="Delete" CommandName="Delete" />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:Button ID="btnupdate" runat="server" Text="Update" CommandName="Update" />
                                                <asp:Button ID="btnDelete" runat="server" Text="Calcel" CommandName="Cancel" />
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</asp:Content>

