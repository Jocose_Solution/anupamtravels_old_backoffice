﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class DetailsPort_Agent_AgentTypeDetails
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        CType(Page.Master.FindControl("lblBC"), Label).Text = "<a title='' class='tip-bottom' href=" + ResolveClientUrl("~/DetailsPort/PrivilegePanel/Splashboard.aspx") + " data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Profile</a><a class='current' href='#'>Group Type Details</a>"
        Dim AgentID As String = ""
        Dim msg As String = ""
        LableMsg.Text = msg
        If Not Page.IsPostBack Then

            'DropDownListType.DataSource = GroupTypeMGMT("", TextBoxDesc.Text.Trim(), "MultipleSelect", msg)
            'DropDownListType.DataTextField = "GroupType"
            'DropDownListType.DataValueField = "GroupType"
            'DropDownListType.DataBind()

            BindData()

        End If



    End Sub
    Protected Sub ButtonSubmit_Click(sender As Object, e As EventArgs) Handles ButtonSubmit.Click

        Dim msg As String = ""
        Try
            GroupTypeMGMT(TextBoxGroupType.Text.Trim().Replace(" ", [String].Empty), TextBoxDesc.Text.Trim(), "Insert", msg)

            If msg = "ADDED" Then
                msg = "Group Type successfully inserted."
            Else
                msg = "Try again"
            End If
            LableMsg.Text = msg
            BindData()

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Private Sub BindData()
        Dim msg As String = ""
        Try

            GridView1.DataSource = GroupTypeMGMT("", TextBoxDesc.Text.Trim(), "MultipleSelect", msg)
            GridView1.DataBind()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub

    Public Function GroupTypeMGMT(ByVal type As String, ByVal desc As String, ByVal cmdType As String, ByRef msg As String) As DataTable
        Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
        Dim dt As New DataTable()
        Try

            con.Open()

            Dim cmd As New SqlCommand()

            cmd.CommandText = "usp_agentTypeMGMT"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@UserType", SqlDbType.VarChar, 200).Value = type
            cmd.Parameters.Add("@desc", SqlDbType.VarChar, 500).Value = desc
            cmd.Parameters.Add("@cmdType", SqlDbType.VarChar, 50).Value = cmdType
            cmd.Parameters.Add("@msg", SqlDbType.VarChar, 500)
            cmd.Parameters("@msg").Direction = ParameterDirection.Output

            cmd.Connection = con
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)
            msg = cmd.Parameters("@msg").Value.ToString().Trim()



            con.Close()


        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
            con.Close()

        End Try
        Return dt
    End Function

    Protected Sub GridView1_RowCancelingEdit(sender As Object, e As GridViewCancelEditEventArgs) Handles GridView1.RowCancelingEdit
        Try
            GridView1.EditIndex = -1
            BindData()

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub

    Protected Sub GridView1_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles GridView1.RowEditing

        Try
            GridView1.EditIndex = e.NewEditIndex
            BindData()

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub

    Protected Sub GridView1_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles GridView1.RowDeleting
        Dim msg As String = ""
        Try
            Dim userType As String = UCase(DirectCast(GridView1.Rows(e.RowIndex).FindControl("LableGroupType"), Label).Text.ToUpper)
            GroupTypeMGMT(userType, "", "Delete", msg)

            If msg = "1" Then
                msg = "Group Type succesfully deleted."
            Else

            End If
            LableMsg.Text = msg
            BindData()

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try

    End Sub

    Protected Sub GridView1_RowUpdating(sender As Object, e As GridViewUpdateEventArgs) Handles GridView1.RowUpdating

        Dim msg As String = ""
        Try

            If DirectCast(GridView1.Rows(e.RowIndex).Cells(0).Controls(0), LinkButton).Text = "Update" Then

                Dim userType As String = UCase(DirectCast(GridView1.Rows(e.RowIndex).FindControl("LableGroupType"), Label).Text.ToUpper)
                Dim desc As String = UCase(DirectCast(GridView1.Rows(e.RowIndex).FindControl("TextBoxDesc"), TextBox).Text.ToUpper)

                GroupTypeMGMT(userType.Trim(), desc.Trim(), "Update", msg)

            End If
            GridView1.EditIndex = -1
            BindData()

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub
End Class
