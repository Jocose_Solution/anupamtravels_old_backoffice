﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="INTask.aspx.cs" Inherits="FlightReports_INTask" %>

<%@ Register Src="~/FlightReports/Int_task/IntlHoldPNRRequest.ascx" TagPrefix="Search" TagName="IntlHoldPNRRequest" %>
<%@ Register Src="~/FlightReports/Int_task/IntlHoldPNRUpdate.ascx" TagPrefix="Search1" TagName="IntlHoldPNRUpdate" %>
<%@ Register Src="~/FlightReports/Int_task/TktRptIntl_RefundInProcess.ascx" TagPrefix="Search2" TagName="TktRptIntl_RefundInProcess" %>
<%@ Register Src="~/FlightReports/Int_task/TktRptIntl_RefundRequest.ascx" TagPrefix="Search3" TagName="TktRptIntl_RefundRequest" %>
<%@ Register Src="~/FlightReports/Int_task/TktRptIntl_ReIssueInProcess.ascx" TagPrefix="Search4" TagName="TktRptIntl_ReIssueInProcess" %>
<%@ Register Src="~/FlightReports/Int_task/TktRptIntl_ReIssueRequest.ascx" TagPrefix="Search5" TagName="TktRptIntl_ReIssueRequest" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 <%--    <script type="text/javascript">
         var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>--%>
            <div class="col-md-12 container-fluid" style="padding-right: 35px">
            <div class="page-wrapperss">

                <div class="panel panel-primary">

                    <div class="panel-body">
    <div class="container">

  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">Cancellation Report</a></li>
    <li><a data-toggle="tab" href="#menu1">Hold PNR Report</a></li>
    <li><a data-toggle="tab" href="#menu2">QC Ticket Report</a></li>
       <li><a data-toggle="tab" href="#menu3">Reissue Report</a></li>
       <li><a data-toggle="tab" href="#menu4">Rejected Ticket</a></li>
       <li><a data-toggle="tab" href="#menu5">Requested Request</a></li>
       
  </ul>

  <div class="tab-content" style="overflow:hidden;">
    <div id="home" class="tab-pane fade in active">
     
     <Search:IntlHoldPNRRequest ID="IntlHoldPNRRequest1" runat="server" />
    </div>
      <div id="menu1" class="tab-pane fade">

          <Search1:IntlHoldPNRUpdate runat="server" ID="IntlHoldPNRUpdate1" />
      </div>
    <div id="menu2" class="tab-pane fade">
    
    <Search2:TktRptIntl_RefundInProcess runat="server" ID="TktRptIntl_RefundInProcess1" />
    </div>

      <div id="menu3" class="tab-pane fade">
    
    <Search3:TktRptIntl_RefundRequest runat="server" ID="TktRptIntl_RefundRequest1" />
    </div>

      <div id="menu4" class="tab-pane fade">
    
    <Search4:TktRptIntl_ReIssueInProcess runat="server" ID="TktRptIntl_ReIssueInProcess1" />
    </div>

      <div id="menu5" class="tab-pane fade">
    
    <Search5:TktRptIntl_ReIssueRequest runat="server" ID="TktRptIntl_ReIssueRequest1" />
    </div>

   

  </div>
</div>
                        </div>
                    </div>
                </div>

            </div>


     
  




    
   
     
   <%--  <script type="text/javascript">
         jQuery.noConflict();
         $(document).ready(function () {
             $("#tabs").tabs();
         });
         </script>--%>

</asp:Content>

