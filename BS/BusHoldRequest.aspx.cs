﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
//using PG;

public partial class BS_BusHoldRequest : System.Web.UI.Page
{

    //RefundResponse objRefnResp = new RefundResponse();
    //_CrOrDb objParamCrd = new _CrOrDb();
    //ITZcrdb objCrd = new ITZcrdb();
    //ITZ_Trans objIzT = new ITZ_Trans();
    //Itz_Trans_Dal objItzT = new Itz_Trans_Dal();
    //ITZGetbalance objItzBal = new ITZGetbalance();
    //_GetBalance objParamBal = new _GetBalance();
    //GetBalanceResponse objBalResp = new GetBalanceResponse();
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        try
        {
            if (string.IsNullOrEmpty(Session["UID"].ToString()) | Session["UID"] == null)
            {
                Response.Redirect("~/Login.aspx");
            }

            if (IsPostBack)
            {
            }
            else
            {
                BindGrid();
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);

        }
    }
    public void BindGrid()
    {
        try
        {
           GrdBusHoldReport.DataSource = BUSDetails();
           GrdBusHoldReport.DataBind();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }

    }
    public DataSet BUSDetails()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myCon"].ConnectionString);
        DataSet DS = new DataSet();
        try
        {
            using (SqlCommand sqlcmd = new SqlCommand())
            {
                sqlcmd.Connection = con;
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                else
                {
                    con.Open();
                }
                sqlcmd.CommandTimeout = 900;
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandText = "SP_BUSHoldRequestReport";               
                SqlDataAdapter da = new SqlDataAdapter(sqlcmd);
                da.Fill(DS);
               
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        finally
        {
           
            DS.Dispose();
            con.Close();
        }
        return DS;
    }
    protected void RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Accept")
            {
                try
                {
                    ViewState["OrderId"] = e.CommandArgument.ToString();
                    LinkButton lb = e.CommandSource as LinkButton;
                    GridViewRow gvr = lb.Parent.Parent as GridViewRow;
                    int RowIndex = gvr.RowIndex;
                    ViewState["RowIndex"] = RowIndex;
                    Label seatno = (Label)GrdBusHoldReport.Rows[RowIndex].FindControl("lbl_SEATNO");                
                    UpdateBuscanceldetail(Convert.ToString(ViewState["OrderId"]), Convert.ToInt32(seatno.Text), "B_A", "InProcess", "");                                      
                    gvr.BackColor = System.Drawing.Color.GreenYellow;
                    BindGrid();
                }
                catch (Exception ex)
                {
                    clsErrorLog.LogInfo(ex);
                }

            }
            else if (e.CommandName == "Reject")
            {
                try
                {
                    GrdBusHoldReport.Columns[14].Visible = false;                                
                    LinkButton lb = e.CommandSource as LinkButton;
                    GridViewRow gvr = lb.Parent.Parent as GridViewRow;
                    int RowIndex = gvr.RowIndex;
                    ViewState["RowIndex"] = RowIndex;
                    TextBox txtRemark = (TextBox)GrdBusHoldReport.Rows[RowIndex].FindControl("txtRemark");
                    LinkButton lnkSubmit = (LinkButton)GrdBusHoldReport.Rows[RowIndex].FindControl("lnkSubmit");
                    LinkButton lnkHides = (LinkButton)GrdBusHoldReport.Rows[RowIndex].FindControl("lnkHides");
                    lnkHides.Visible = true;
                    txtRemark.Visible = true;
                    lnkSubmit.Visible = true;
                    gvr.BackColor = System.Drawing.Color.Yellow;
                }
                catch (Exception ex)
                {
                    clsErrorLog.LogInfo(ex);

                }

            }
            else if (e.CommandName == "lnkHides")
            {
                try
                {

                    
                    LinkButton lb = e.CommandSource as LinkButton;
                    GridViewRow gvr = lb.Parent.Parent as GridViewRow;
                    int RowIndex = gvr.RowIndex;
                    ViewState["RowIndex"] = RowIndex;
                    TextBox txtRemark = (TextBox)GrdBusHoldReport.Rows[RowIndex].FindControl("txtRemark");
                    LinkButton lnkSubmit = (LinkButton)GrdBusHoldReport.Rows[RowIndex].FindControl("lnkSubmit");
                    LinkButton lnkHides = (LinkButton)GrdBusHoldReport.Rows[RowIndex].FindControl("lnkHides");
                    lnkHides.Visible = false;
                    txtRemark.Visible = false;
                    lnkSubmit.Visible = false;
                    gvr.BackColor = System.Drawing.Color.White;

                }
                catch (Exception ex)
                {
                    clsErrorLog.LogInfo(ex);

                }

            }
            else if (e.CommandName == "submit")
            {
                LinkButton lb = e.CommandSource as LinkButton;
                GridViewRow gvr = lb.Parent.Parent as GridViewRow;
                int RowIndex = gvr.RowIndex;
                ViewState["RowIndex"] = RowIndex;
                TextBox txtRemark = (TextBox)GrdBusHoldReport.Rows[RowIndex].FindControl("txtRemark");
                if (txtRemark.Text == "")
                {
                    ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('please Provide Remark.');javascript: window.opener.location=window.opener.location.href;", true);

                }
                else
                {
                    try
                    {
                        int i = 0;
                        ViewState["OrderId"] = e.CommandArgument.ToString();                      
                        Label seatno = (Label)GrdBusHoldReport.Rows[RowIndex].FindControl("lbl_SEATNO");                                              
                        LinkButton lnkSubmit = (LinkButton)GrdBusHoldReport.Rows[RowIndex].FindControl("lnkSubmit");
                        LinkButton lnkHides = (LinkButton)GrdBusHoldReport.Rows[RowIndex].FindControl("lnkHides");
                        lnkHides.Visible = false;
                        txtRemark.Visible = false;
                        lnkSubmit.Visible = false;
                        gvr.BackColor = System.Drawing.Color.White;
                        //i = UpdateBuscanceldetail(Convert.ToString(ViewState["OrderId"]), Convert.ToInt32(seatno.Text), "B_R", "Rejected", txtRemark.Text);
                        //if (i > 0)
                        //{
                        //    ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Rejected Successfully.');javascript: window.close();window.opener.location=window.opener.location.href;", true);
                        //    BindGrid();
                        //}
                        //else
                        //{
                        //    ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Try Again.');javascript: window.close();window.opener.location=window.opener.location.href;", true);
                        //    BindGrid();

                        //}
                        Label lbl_Agent_ID = (Label)GrdBusHoldReport.Rows[RowIndex].FindControl("lbl_Agent_ID");
                        Label lbl_ORDERID = (Label)GrdBusHoldReport.Rows[RowIndex].FindControl("lbl_ORDERID");
                        Label refundamt = (Label)GrdBusHoldReport.Rows[RowIndex].FindControl("lbl_TA_NET_FARE");
                        Label bookamt = (Label)GrdBusHoldReport.Rows[RowIndex].FindControl("lbl_TA_NET_FARE");
                        Label refno = (Label)GrdBusHoldReport.Rows[RowIndex].FindControl("lbl_BOOKING_REF");
                        Boolean result = AutoRefund(lbl_ORDERID.Text, Convert.ToDouble(refundamt.Text), lbl_Agent_ID.Text, bookamt.Text, refno.Text);
                        if (result == true)
                        {
                            UpdateBuscanceldetail(Convert.ToString(ViewState["OrderId"]), Convert.ToInt32(seatno.Text), "B_R", "Rejected", txtRemark.Text);
                            ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Rejected Successfully.');javascript: window.close();window.opener.location=window.opener.location.href;", true);
                            BindGrid();
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Try Again.');javascript: window.close();window.opener.location=window.opener.location.href;", true);
                            BindGrid();
                        }
                        GrdBusHoldReport.Columns[14].Visible = true;   
                    }
                    catch (Exception ex)
                    {
                        clsErrorLog.LogInfo(ex);

                    }
                }

            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);

        }
    }
    protected void btnCanFee_Click(object sender, System.EventArgs e)
    {
        try
        {
            int RowIndex = Convert.ToInt32(ViewState["RowIndex"]);
            Label seatno = (Label)GrdBusHoldReport.Rows[RowIndex].FindControl("lbl_SEATNO"); 
            TextBox txtRemark = (TextBox)GrdBusHoldReport.Rows[RowIndex].FindControl("txtRemark");
            Label OrderId = (Label)GrdBusHoldReport.Rows[RowIndex].FindControl("OrderId");
            Label PnrNo = (Label)GrdBusHoldReport.Rows[RowIndex].FindControl("GdsPNR");
            Label Pax = (Label)GrdBusHoldReport.Rows[RowIndex].FindControl("PaxType");
            Label NetAmount = (Label)GrdBusHoldReport.Rows[RowIndex].FindControl("lbltotalfare");
            IntlDetails ObjIntDetails = new IntlDetails();
            //UpdateBuscanceldetail(Convert.ToInt32(ViewState["Counter"]), seatno, "B_C", "Rejected", txtRemark.Text);
            UpdateBuscanceldetail(Convert.ToString(ViewState["OrderId"]), Convert.ToInt32(seatno.Text), "B_R", "Rejected", txtRemark.Text);  
            DataTable dt = new DataTable();
            string strMailMsgHold = null;
            strMailMsgHold = "<table>";
            strMailMsgHold = strMailMsgHold + "<tr>";
            strMailMsgHold = strMailMsgHold + "<td><h2> Refund Request Booking Summary </h2>";
            strMailMsgHold = strMailMsgHold + "</td>";
            strMailMsgHold = strMailMsgHold + "</tr>";
            strMailMsgHold = strMailMsgHold + "<tr>";
            strMailMsgHold = strMailMsgHold + "</tr>";
            strMailMsgHold = strMailMsgHold + "<tr>";
            strMailMsgHold = strMailMsgHold + "<td><b>Order ID: </b>" + Convert.ToString(OrderId.Text);
            strMailMsgHold = strMailMsgHold + "</td>";
            strMailMsgHold = strMailMsgHold + "</tr>";
            strMailMsgHold = strMailMsgHold + "<tr>";
            strMailMsgHold = strMailMsgHold + "<td><b>PnrNo: </b>";
            strMailMsgHold = strMailMsgHold + "</td>";
            strMailMsgHold = strMailMsgHold + "</tr>";
            strMailMsgHold = strMailMsgHold + "<tr>";
            strMailMsgHold = strMailMsgHold + "<td><b>Pax Name: </b>";
            strMailMsgHold = strMailMsgHold + "</td>";
            strMailMsgHold = strMailMsgHold + "</tr>";
            strMailMsgHold = strMailMsgHold + "<tr>";
            strMailMsgHold = strMailMsgHold + "<td><b>Pax Type: </b>";
            strMailMsgHold = strMailMsgHold + "</td>";
            strMailMsgHold = strMailMsgHold + "</tr>";
            strMailMsgHold = strMailMsgHold + "<tr>";
            strMailMsgHold = strMailMsgHold + "<td><b>Net Amount: </b>";
            strMailMsgHold = strMailMsgHold + "</td>";
            strMailMsgHold = strMailMsgHold + "</tr>";
            strMailMsgHold = strMailMsgHold + "<tr>";
            strMailMsgHold = strMailMsgHold + "<td><b>Remark: </b>";
            strMailMsgHold = strMailMsgHold + "</td>";
            strMailMsgHold = strMailMsgHold + "</tr>";
            strMailMsgHold = strMailMsgHold + "</table>";
            dt = Email_Credentilas(OrderId.Text.Trim().ToString(), "C_REJECTED", ViewState["Counter"].ToString());
            SqlTransactionDom STDOM = new SqlTransactionDom();
            DataTable MailDt = new DataTable();
            MailDt = STDOM.GetMailingDetails(MAILING.AIR_PNRSUMMARY.ToString(), Session["UID"].ToString()).Tables[0];
            try
            {
                if ((MailDt.Rows.Count > 0))
                {
                    for (int j = 0; j <= dt.Rows.Count - 1; j++)
                    {
                        STDOM.SendMail(dt.Rows[j][1].ToString(), "info@itztravel.itzcash.com", "", MailDt.Rows[0]["CC"].ToString(), MailDt.Rows[0]["SMTPCLIENT"].ToString(), MailDt.Rows[0]["UserId"].ToString(), MailDt.Rows[0]["Pass"].ToString(), strMailMsgHold, "Ticket Reissue Request", "");
                    }
                }

            }
            catch (Exception ex)
            {
                clsErrorLog.LogInfo(ex);
            }
            ShowAlertMessage("Reject successfully");
            BindGrid();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
    protected void lnkHides_Click(object sender, System.EventArgs e)
    {
        try
        {
            GrdBusHoldReport.Columns[14].Visible = true;   
            BindGrid();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);

        }
    }
    public int UpdateBuscanceldetail(string orderid, int seatno, string ReqType, string Status, string remark)
    {
        int i = 0;
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myCon"].ConnectionString);
        DataSet DS = new DataSet();
        try
        {
            using (SqlCommand sqlcmd = new SqlCommand())
            {
                sqlcmd.Connection = con;
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                else
                {
                    con.Open();
                }
                sqlcmd.CommandTimeout = 900;
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandText = "USP_BusAccept_Rejecttkt";
                sqlcmd.Parameters.AddWithValue("@orderid", orderid);               
                sqlcmd.Parameters.AddWithValue("@Status", Status);
                sqlcmd.Parameters.AddWithValue("@remark", remark);
                sqlcmd.Parameters.AddWithValue("@seatno", seatno);
                sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);     
                i = sqlcmd.ExecuteNonQuery();
               

            }
        }

        catch (Exception ex)
        {
            
        }
        finally
        {
            con.Close();

        }
        return i;

    }
    public DataTable Email_Credentilas(string Orderid, string Cmd_Type, string Counter)
    {
        SqlConnection con1 = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        DataTable dt = new DataTable();
        try
        {
            con1.Open();
          
            SqlCommand cmd = new SqlCommand();
            //cmd.CommandText = "USP_TICKETSTATUS"
            cmd.CommandText = "USP_TICKETSTATUS_PP";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@ORDERID", SqlDbType.VarChar).Value = Orderid;
            cmd.Parameters.Add("@CMD_TYPE", SqlDbType.VarChar).Value = Cmd_Type;
            cmd.Parameters.Add("@COUNTER", SqlDbType.VarChar).Value = Counter;
            cmd.Connection = con1;
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dt);
        }
        catch (Exception ex)
        {

        }
        finally
        {
            con1.Close();
        }
       
        return dt;
    }
    public static void ShowAlertMessage(string error)
    {
        try
        {
            Page page = HttpContext.Current.Handler as Page;
            if (page != null)
            {
                error = error.Replace("'", "'");
                ScriptManager.RegisterStartupScript(page, page.GetType(), "err_msg", "alert('" + error + "');", true);
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);

        }
    }
    protected bool AutoRefund(string orderid, double RefundAmount, string AgentID, string BookingAmount, string refid)
    {
        string MchntKeyITZ = ConfigurationManager.AppSettings["BUSMerchantKey"].ToString();
        bool Rfndstatus = false;
       
        SqlTransaction ST = new SqlTransaction();
        SqlTransactionDom STDom = new SqlTransactionDom();
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        try
        {
            DataSet AgencyDS = new DataSet();
            AgencyDS = ST.GetAgencyDetails(AgentID);                            
            try
            {
                if (!string.IsNullOrEmpty(AgentID) && !string.IsNullOrEmpty(Convert.ToString(RefundAmount)))
                {
                    Rfndstatus = true;
                    double Aval_Bal = ST.AddCrdLimit(AgentID, RefundAmount);
                    STDom.insertLedgerDetails(AgentID, AgencyDS.Tables[0].Rows[0]["Agency_Name"].ToString(), orderid, refid, "", "", "", "", Session["UID"].ToString(), Request.UserHostAddress,
                    0, RefundAmount, Aval_Bal, "Bus booking Auto Rejection", "Refund Against  orderid=" + orderid, 0);
                }                
            }
            catch (Exception ex)
            {
                clsErrorLog.LogInfo(ex);
            }            
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        return Rfndstatus;
    }  

}