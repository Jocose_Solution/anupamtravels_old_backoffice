﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="registration.aspx.vb" MasterPageFile="~/MasterPageSignIn.master" Inherits="DetailsPort_Admin_registration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link href="http://localhost:59751/maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css" />
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->
    <style type="text/css">
        body {
            background: #fff;
        }

        .rthead {
            font-size: 25px;
            color: #fff;
            background-color: #33abb7;
            width: 100%;
            text-align: center;
            padding: 10px;
        }

        .form-controlrt {
            display: block;
            width: 100%;
            height: 40px !important;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 0px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        }

        /*form {
        width: 60%;
        margin: 60px auto;
        background: #efefef;
        padding: 60px 120px 80px 120px;
        text-align: center;
        -webkit-box-shadow: 2px 2px 3px rgba(0,0,0,0.1);
        box-shadow: 2px 2px 3px rgba(0,0,0,0.1);
    }*/

        label {
            display: block;
            position: relative;
            margin: 40px 0px;
        }

        input {
            width: 100%;
            padding: 10px;
            background: transparent;
            border: none;
            outline: none;
        }

        .line-box {
            position: relative;
            width: 100%;
            height: 2px;
            background: #230303;
        }

        .line {
            position: absolute;
            width: 0%;
            height: 2px;
            top: 0px;
            left: 50%;
            transform: translateX(-50%);
            background: #8BC34A;
            transition: ease .6s;
        }

        input:focus + .line-box .line {
            width: 100%;
        }

        .label-txt {
            position: absolute;
            top: -1.6em;
            padding: 10px;
            font-family: sans-serif;
            font-size: .8em;
            letter-spacing: 1px;
            color: rgb(120,120,120);
            transition: ease .3s;
        }

        .w70 {
            width: 70% !important;
        }

        .label-active {
            top: -3em;
        }

        /*button {
        display: inline-block;
        padding: 12px 24px;
        background: rgb(220,220,220);
        font-weight: bold;
        color: rgb(120,120,120);
        border: none;
        outline: none;
        border-radius: 3px;
        cursor: pointer;
        transition: ease .3s;
    }*/

        button:hover {
            background: #8BC34A;
            color: #ffffff;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {

            $('input').focus(function () {
                $(this).parent().find(".label-txt").addClass('label-active');
            });

            $("input").focusout(function () {
                if ($(this).val() == '') {
                    $(this).parent().find(".label-txt").removeClass('label-active');
                };
            });

        });
    </script>

    <script src="chosen/jquery-1.6.1.min.js" type="text/javascript"></script>

    <script src="chosen/chosen.jquery.js" type="text/javascript"></script>
    <link href="CSS/jquery-ui-1.8.8.custom.css" rel="stylesheet" />


    <script type="text/javascript">
        $(document).ready(function () {
            $(".chzn-select").chosen();
            $(".chzn-select-deselect").chosen({ allow_single_deselect: true });
        });
    </script>

    <script type="text/javascript">
        function RefreshCaptcha() {
            var img = document.getElementById("imgCaptcha");
            img.src = "../../CAPTCHA.ashx?query=" + Math.random();
        }
    </script>
    <script language="javascript" type="text/javascript">
        function validateSearch() {

            if (document.getElementById("ctl00_ContentPlaceHolder1_Fname_txt").value == "") {
                alert('Specify First Name');
                document.getElementById("ctl00_ContentPlaceHolder1_Fname_txt").focus();
                return false;
            }
            if (document.getElementById("ctl00_ContentPlaceHolder1_Lname_txt").value == "") {
                alert('Specify Last Name');
                document.getElementById("ctl00_ContentPlaceHolder1_Lname_txt").focus();
                return false;
            }
            if (document.getElementById("ctl00_ContentPlaceHolder1_Add_txt").value == "") {
                alert('Specify Address');
                document.getElementById("ctl00_ContentPlaceHolder1_Add_txt").focus();
                return false;
            }

            if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_country").value == "India") {
                if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_state").value == "--Select State--") {
                    alert('Please Select State');
                    document.getElementById("ctl00_ContentPlaceHolder1_ddl_state").focus();
                    return false;
                }
                if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_city").value == "") {
                    alert('Please Select City');
                    document.getElementById("ctl00_ContentPlaceHolder1_ddl_city").focus();
                    return false;
                }

            }
            else {
                if (document.getElementById("ctl00_ContentPlaceHolder1_Coun_txt").value == "") {
                    alert('Specify Country Name');
                    document.getElementById("ctl00_ContentPlaceHolder1_Coun_txt").focus();
                    return false;
                }
                if (document.getElementById("ctl00_ContentPlaceHolder1_Stat_txt").value == "") {
                    alert('Specify State Name');
                    document.getElementById("ctl00_ContentPlaceHolder1_Stat_txt").focus();
                    return false;
                }
                if (document.getElementById("ctl00_ContentPlaceHolder1_City_txt").value == "") {
                    alert('Specify City Name');
                    document.getElementById("ctl00_ContentPlaceHolder1_City_txt").focus();
                    return false;
                }
            }
            if (document.getElementById("ctl00_ContentPlaceHolder1_Pin_txt").value == "") {
                alert('Specify Pincode');
                document.getElementById("ctl00_ContentPlaceHolder1_Pin_txt").focus();
                return false;

            }
            if (document.getElementById("ctl00_ContentPlaceHolder1_Mob_txt").value == "") {
                alert('Specify Mobile Number');
                document.getElementById("ctl00_ContentPlaceHolder1_Mob_txt").focus();
                return false;
            }
            if (document.getElementById("ctl00_ContentPlaceHolder1_Email_txt").value == "") {
                alert('Specify EmailID');
                document.getElementById("ctl00_ContentPlaceHolder1_Email_txt").focus();
                return false;
            }

            var emailPat = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+))|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            var emailid = document.getElementById("ctl00_ContentPlaceHolder1_Email_txt").value;
            var matchArray = emailid.match(emailPat);
            if (matchArray == null) {
                alert("Your email address seems incorrect. Please try again.");
                document.getElementById("ctl00_ContentPlaceHolder1_Email_txt").focus();
                return false;
            }

            if (document.getElementById("ctl00_ContentPlaceHolder1_Agn_txt").value == "") {
                alert('Specify Agency Name');
                document.getElementById("ctl00_ContentPlaceHolder1_Agn_txt").focus();
                return false;
            }
            if (document.getElementById("ctl00_ContentPlaceHolder1_TextBox_NameOnPard").value == "") {
                alert('Specify Name on Pan Card');
                document.getElementById("ctl00_ContentPlaceHolder1_TextBox_NameOnPard").focus();
                return false;
            }
            if (document.getElementById("ctl00_ContentPlaceHolder1_Pan_txt").value == "") {
                alert('Specify Pan No');
                document.getElementById("ctl00_ContentPlaceHolder1_Pan_txt").focus();
                return false;
            }
            if (document.getElementById("ctl00_ContentPlaceHolder1_Ans_txt").value == "") {
                alert('Specify Answer');
                document.getElementById("ctl00_ContentPlaceHolder1_Ans_txt").focus();
                return false;
            }


            if (document.getElementById("ctl00_ContentPlaceHolder1_TxtUserId").value == "") {
                alert('Specify Userid');
                document.getElementById("ctl00_ContentPlaceHolder1_TxtUserId").focus();
                return false;
            }

            if (document.getElementById("ctl00_ContentPlaceHolder1_Pass_text").value == "") {
                alert('Specify Password');
                document.getElementById("ctl00_ContentPlaceHolder1_Pass_text").focus();
                return false;
            }
            else {
                var regex = /^(?=.[a-z])(?=.[A-Z])(?=.\d)(?=.[#$@!%&?])[A-Za-z\d#$@!%&?]{8,16}$/;
                if (!regex.test(document.getElementById("ctl00_ContentPlaceHolder1_Pass_text").value)) {
                    alert("Password must contain:8 To 16 characters atleast 1 UpperCase Alphabet, 1 LowerCase Alphabet, 1 Number and 1 Special Character'");
                    document.getElementById("ctl00_ContentPlaceHolder1_Pass_text").focus();
                    return false;
                }


                if (document.getElementById("ctl00_ContentPlaceHolder1_cpass_txt").value == "") {
                    alert('Specify Confirm Password');
                    document.getElementById("ctl00_ContentPlaceHolder1_cpass_txt").focus();
                    return false;
                }

                if (document.getElementById("ctl00_ContentPlaceHolder1_cpass_txt").value != "") {

                    if (document.getElementById("ctl00_ContentPlaceHolder1_Pass_text").value != document.getElementById("ctl00_ContentPlaceHolder1_cpass_txt").value) {
                        alert("Confirm Password is same as Password");
                        document.getElementById("ctl00_ContentPlaceHolder1_cpass_txt").focus();
                        return false;
                    }

                    if (confirm("Are you sure!"))
                        return true;
                    return false;

                }
            }

            function phone_vali() {
                if ((event.keyCode > 47 && event.keyCode < 58) || (event.keyCode == 32) || (event.keyCode == 45))
                    event.returnValue = true;
                else
                    event.returnValue = false;
            }
            function vali() {
                if ((event.keyCode > 64 && event.keyCode < 91) || (event.keyCode > 96 && event.keyCode < 123) || (event.keyCode == 32) || (event.keyCode == 45))
                    event.returnValue = true;
                else
                    event.returnValue = false;
            }

            function vali1() {
                if ((event.keyCode > 64 && event.keyCode < 91) || (event.keyCode > 96 && event.keyCode < 123) || (event.keyCode == 32) || (event.keyCode > 47 && event.keyCode < 58) || (event.keyCode == 32))
                    event.returnValue = true;
                else
                    event.returnValue = false;
            }

            function getKeyCode(e) {
                if (window.event)
                    return window.event.keyCode;
                else if (e)
                    return e.which;
                else
                    return null;
            }
            function keyRestrict(e, validchars) {
                var key = '', keychar = '';
                key = getKeyCode(e);
                if (key == null) return true;
                keychar = String.fromCharCode(key);
                keychar = keychar.toLowerCase();
                validchars = validchars.toLowerCase();
                if (validchars.indexOf(keychar) != -1)
                    return true;
                if (key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27)
                    return true;
                return false;
            }
        }
    </script>

         <div class="col-md-12 container-fluid" style="padding-right: 35px">
            <div class="page-wrapperss">

                <div class="panel panel-primary">

                    <div class="panel-body">
            <div class="row rtmainbgs">
                <div class="rthead">CUSTOMER REGISTER</div>



                <div class="w100" id="table_reg" runat="server"
                    visible="true" style="margin-top: 0px; padding-bottom: 20px;">
                    <div class="row">
                        <div class="container-fluid" style="margin-left: 75px">
                            <div class="heading w100">
                                <div align="center">
                                    <asp:Label ID="lbl_msg" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="20px"
                                        ForeColor="#FF3300"></asp:Label>
                                </div>
                                <br />
                                <div class="clear1"></div>
                                <div class="w100">
                                    <div class="row">
                                        <div class="clearfix"></div>
                                        <div class="col-md-2" style="padding-top: 25px">
                                            Title
                                        <asp:DropDownList ID="tit_drop" runat="server" CssClass="form-control input-text full-width">
                                            <asp:ListItem Value="" disabled Selected style="display: none;">Select Title</asp:ListItem>
                                            <asp:ListItem Value="Mr.">Mr.</asp:ListItem>
                                            <asp:ListItem Value="Ms.">Ms.</asp:ListItem>
                                            <asp:ListItem Value="Mrs.">Mrs.</asp:ListItem>
                                        </asp:DropDownList>
                                        </div>


                                        <div class="col-md-2">
                                            <label>
                                                <p class="label-txt" style="font-size: 12px">First Name</p>
                                                <input type="text" class="abcd" id="Fname_txt" style="position: static" onkeypress="return keyRestrict(event,' abcdefghijklmnopqrstuvwxyz');" maxlength="50" />
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                        </div>



                                        <div class="col-md-2">
                                            <label>
                                                <p class="label-txt">Surname</p>
                                                <input type="text" class="input" id="Lname_txt" style="position: static"
                                                    onkeypress="return keyRestrict(event,' abcdefghijklmnopqrstuvwxyz');" maxlength="50" required="" />
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                        </div>




                                        <div class="col-md-2">
                                            <label>
                                                <p class="label-txt">Address</p>
                                                <input type="text" class="input" id="Add_txt" style="height: 35px;" />
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                        </div>




                                        <div class="col-md-2" style="padding-top: 25px">
                                            Country                                       
         <asp:DropDownList ID="ddl_country" runat="server" CssClass="form-control input-text full-width" AutoPostBack="True">
             <asp:ListItem Selected="True" Value="India">India</asp:ListItem>
             <asp:ListItem Value="Other">Other</asp:ListItem>
         </asp:DropDownList>
                                            <asp:TextBox ID="Coun_txt" CssClass="psb_dd input-text full-width " runat="server" MaxLength="30" Style="position: static"
                                                onkeypress="return keyRestrict(event,'abcdefghijklmnopqrstuvwxyz');" Visible="true"></asp:TextBox>
                                        </div>



                                        <div class="col-md-2" style="padding-top: 25px">
                                            State
                                <asp:DropDownList ID="ddl_state" runat="server" AutoPostBack="True" CssClass="form-control input-text full-width">
                                    <asp:ListItem Value="State">State</asp:ListItem>
                                </asp:DropDownList>
                                            <asp:TextBox ID="Stat_txt" CssClass="psb_dd input-text full-width" runat="server" Style="position: static"
                                                onkeypress="return vali();" Visible="true"></asp:TextBox>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-2" style="margin-top: -30px">
                                            <label>
                                                <p class="label-txt">City</p>
                                                <input type="text" id="ddl_city" runat="server" class="psb_dd input-text full-width" required="" />
                                                <asp:TextBox ID="Other_City" CssClass="psb_dd input-text full-width" runat="server" Style="position: static"
                                                    onkeypress="return vali();" Visible="true"></asp:TextBox>
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                        </div>



                                        <div class="col-md-2">
                                            <label>
                                                <p class="label-txt">Area</p>
                                                <input type="text" class="input" id="TextBox_Area" ccsclass="psb_dd input-text full-width" runat="server" maxlength="30" style="position: static" />
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                        </div>



                                        <div class="col-md-2">
                                            <label>
                                                <p class="label-txt">PinCode</p>
                                                <input type="text" class="input" id="Pin_txt" style="position: static"
                                                    onkeypress="return keyRestrict(event,'1234567890');" maxlength="8" required="" />
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                        </div>





                                        <div class="col-md-2">
                                            <label>
                                                <p class="label-txt">Phon No</p>
                                                <input type="text" id="Ph_txt" class="input" style="position: static"
                                                    onkeypress="return keyRestrict(event,'0123456789');" maxlength="12" required="" />
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                        </div>




                                        <div class="col-md-2">
                                            <label>
                                                <p class="label-txt">Mobile No</p>
                                                <input type="text" id="Mob_txt" class="input" style="position: static"
                                                    onkeypress="return keyRestrict(event,'0123456789');" maxlength="10" />
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                        </div>




                                        <div class="col-md-2">
                                            <label>
                                                <p class="label-txt">Email</p>
                                                <input type="text" id="Email_txt" class="input" style="position: static" maxlength="50" required="" />
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                        </div>
                                    </div>



                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>
                                                <p class="label-txt">Alternative Email</p>
                                                <input type="text" id="Aemail_txt" class="input" style="position: static" maxlength="50" required="" />
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                        </div>




                                        <div class="col-md-2">
                                            <label>
                                                <p class="label-txt">Fax No</p>
                                                <input type="text" id="Fax_txt" class="input" style="position: static" maxlength="40" required="" />
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                        </div>

                                    </div>
                                    <br />
                                    <div class="w100">
                                        <div class="rthead">Brand Information</div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>
                                                    <p class="label-txt">Agency Name</p>
                                                    <input type="text" id="Agn_txt" required="" class="input" maxlength="50" onkeypress="return keyRestrict(event,' abcdefghijklmnopqrstuvwxyz0123456789');" />
                                                    <div class="line-box">
                                                        <div class="line"></div>
                                                    </div>
                                                </label>
                                            </div>






                                            <div class="col-md-2" style="padding-top: 25px">
                                                Agent Type
                                    <asp:DropDownList ID="Stat_drop" CssClass="form-control input-text full-width" runat="server">
                                        <asp:ListItem Value="TA" Selected="True">Travel Agent</asp:ListItem>
                                        <asp:ListItem Value="DI">Stockist</asp:ListItem>
                                    </asp:DropDownList>
                                                <asp:TextBox ID="Web_txt" CssClass="psb_dd input-text full-width" runat="server" Style="position: static; display: none;"></asp:TextBox>
                                            </div>


                                            <div class="col-md-2">
                                                <label>

                                                    <p class="label-txt">Stax No</p>
                                                    <input type="text" class="input" id="Stax_txt" style="position: static" />
                                                    <div class="line-box">
                                                        <div class="line"></div>
                                                    </div>
                                                </label>
                                            </div>


                                            <div class="sclear1"></div>



                                            <div class="col-md-2">
                                                <label>
                                                    <p class="label-txt">Name On Pan Card</p>
                                                    <input type="text" class="input" id="TextBox_NameOnPard" sstyle="position: static" required="" />
                                                    <div class="line-box">
                                                        <div class="line"></div>
                                                    </div>
                                                </label>
                                            </div>



                                            <div class="col-md-2">
                                                <label>
                                                    <p class="label-txt">Pan No</p>
                                                    <input type="text" class="input" id="Pan_txt" style="position: static" required="" />
                                                    <div class="line-box">
                                                        <div class="line"></div>
                                                    </div>
                                                </label>
                                            </div>


                                            <div class="col-md-2">
                                                <label>
                                                    <p class="label-txt">Pan Image</p>
                                                    <asp:FileUpload ID="fld_pan" runat="server" CssClass="psb_dd" Height="22px" />
                                                    <div class="" style="font-size: 11px; color: #ff0000; font-weight: bold">
                                                        <br />
                                                        ( Pancard image must be in JPG formate )
                                                    </div>
                                                </label>
                                            </div>




                                            <div class="col-md-2">
                                                <label>
                                                    <p class="label-txt">Remark</p>
                                                    <input type="text" id="Rem_txt" class="input" style="position: static" required="" />
                                                    <div class="line-box">
                                                        <div class="line"></div>
                                                    </div>
                                                </label>
                                            </div>


                                            <div class="col-md-2" style="padding-top: 25px">
                                                Ref.By
                                    <asp:DropDownList ID="Sales_DDL" runat="server" CssClass="form-control input-text full-width">
                                    </asp:DropDownList>
                                            </div>


                                            <div class="col-md-2" style="padding-top: 25px">
                                                <p class="label-txt"></p>
                                                Upload Logo
               <asp:FileUpload ID="fld_1" runat="server" CssClass="psb_dd" Height="22px" />
                                                <div class="" style="font-size: 11px; color: #ff0000; font-weight: bold">
                                                    <br />
                                                    (Image must be in JPG formate and
                                                                Size should be (9070) pixels)
                                                </div>

                                            </div>


                                            <%--  <div class="clear1"></div>--%>

                                            <div class="col-md-2">Security</div>
                                            <div class="col-md-2" style="padding-top: 60px; margin-left: -212px">
                                                <asp:DropDownList ID="SecQ_drop" CssClass="input-text full-width" runat="server">
                                                    <asp:ListItem Value="What is Your Pet Name?">Mr.What is Your Pet Name?</asp:ListItem>
                                                    <asp:ListItem Value="What is your Favourite Color?">What is your Favourite Color?</asp:ListItem>
                                                    <asp:ListItem Value="What is Your Date of Birth">What is Your Date of Birth</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>



                                            <div class="col-md-2" style="margin-left: 112px">
                                                <label>
                                                    <p class="label-txt">
                                                        Security Answer
                                                    </p>
                                                    <asp:TextBox ID="Ans_txt" CssClass="psb_dd input-text full-width" runat="server" Style="position: static" required=""></asp:TextBox>
                                                    <div class="line-box">
                                                        <div class="line"></div>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>





                                        <div class="w100">
                                            <div class="rthead" >Authentication Information</div>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label>
                                                        <p class="label-txt">User Id</p>
                                                        <input type="text" id="TxtUserId" class="input" required="" style="position: static" maxlength="20" onpaste="return false" onkeypress="return keyRestrict(event,'abcdefghijklmnopqrstuvwxyz1234567890');" />
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                </div>



                                                <div class="col-md-2">
                                                    <label>
                                                        <p class="label-txt">Password</p>
                                                        <input type="text" id="Pass_text" class="input" required="" style="position: static" textmode="Password" maxlength="16" cssclass="psb_dd input-text full-width" />
                                                        <div style="font-size: 11px; color: #0e4faa; font-weight: bold">


                                                            <div class="line-box">
                                                                Eg. abc@123(not more than 16 charecter)
                                                            </div>
                                                            <div class="line"></div>

                                                        </div>
                                                    </label>
                                                </div>

                                                <div class="col-md-2">
                                                    <label>

                                                        <p class="label-txt">Confirm Password</p>
                                                        <input type="text" id="cpass_txt" class="input" required="" style="position: static" textmode="Password" maxlength="16" cssclass="psb_dd input-text full-width">
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                </div>

                                                <div class="clear1"></div>
                                                <label>
                                                    <p class="label-txt">Captcha Information</p>
                                                    <br />
                                                    <div class="col-md-2">
                                                        <%--  <a href="../../CAPTCHA.ashx">../../CAPTCHA.ashx</a>--%>
                                                        <img src="../../CAPTCHA.ashx" id="imgCaptcha" />
                                                        &nbsp;<a onclick="javascript:RefreshCaptcha();" style="cursor: pointer"><img src="../../Images/refresh.png" /></a>
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </div>
                                                </label>



                                                <div class="col-md-2" style="">
                                                    <label>
                                                        <p class="label-txt">Enter Text from Image</p>
                                                        <asp:TextBox ID="TextBox1" CssClass="input-text full-width" runat="server" required=""></asp:TextBox>
                                                        <div class="line-box">
                                                        </div>
                                                    </label>
                                                </div>




                                                <div class="col-md-2">
                                                    <div id="Div1" class="form-group" runat="server">
                                                        <br />
                                                        <asp:Button ID="submit" runat="server" Width="150" runat="server" Text="Submit" OnClientClick="return validateSearch()"
                                                            CssClass="btn btn-success" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear1">
                    </div>
                </div>
            </div>
</div>
                    </div>
                </div>
             </div>
    



            <div id="table_Message" runat="server" visible="false">

                <div class="autoss">
                    Thanks You....!
                </div>

                <div class="w80 auto">
                    <div class="w100">


                        <div class="regss">
                            <b>User Id is : - </b>
                            <%-- <%=CID%>  <br />Agent successfully registered.<br />
                            <%=CID%> is still inactive.  <br />--%>
                        </div>
                        <div class="clear1"></div>

                        <div class="regss">
                            <b>Please activate agent user id.</b>
                        </div>


                    </div>

                </div>

            </div>
       
   
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.7.1.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>









</asp:Content>
