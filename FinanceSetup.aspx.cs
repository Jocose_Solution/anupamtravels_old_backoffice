﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CommissionSetup : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ((Label)Page.Master.FindControl("lblBC")).Text = "<a title='' class='tip-bottom' href=" + ResolveClientUrl("~/DetailsPort/PrivilegePanel/Splashboard.aspx") + " data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Flight Setting </a><a class='current' href='#'>Financial</a>";

        if (Page.Request.QueryString.Keys.Count > 0)
        {
            if (Page.Request.QueryString["r"] == "Dlmst")
                addUC("~/Reports/Commission_Setup/DealCodeMaster.ascx");
            if (Page.Request.QueryString["r"] == "msc_chrg")
                addUC("~/Reports/Commission_Setup/MISCSRVCHARGE.ascx");
            if (Page.Request.QueryString["r"] == "dl_tf")
                addUC("~/Reports/Commission_Setup/DealTransfer.ascx");
            if (Page.Request.QueryString["r"] == "Crd_info")
                addUC("~/Reports/Commission_Setup/AddCreditCard.ascx");
            else if (Page.Request.QueryString["r"]=="comm_mst")
                addUC("~/Reports/Commission_Setup/commissionmaster.ascx");
        }
        else
        {
            addUC("~/Reports/Commission_Setup/commissionmaster.ascx");
        }
    }
    private void addUC(string path)
    {
        UserControl myControl = (UserControl)Page.LoadControl(path);

        //UserControlHolder is a place holder on the aspx page where I want to load the
        //user control to.
        phModule.Controls.Add(myControl);
    }
}